<!DOCTYPE html>
<html>
<?php Flight::render("components/head")?>
<body>
<?php Flight::render("components/navbar")?>

<div class="jumbotron bg-primary jumbotron-fluid text-white">
    <div class="container">
        <h1 class="display-4">Profile</h1>
        <p class="lead">Your profile</p>
    </div>
</div>

<main role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>
                    Uploads
                    <small class="text-muted">All of your fire mixtapes.</small>
                </h3>
            </div>
        </div>
        <?php Flight::render("components/page_selector.php")?>
        <div class="row">
            <div class="col-sm-12">
                <?php Flight::render("components/tracks.php")?>
            </div>
        </div>
    </div>
</main>
</body>
<?php Flight::render("components/footer")?>
</html>