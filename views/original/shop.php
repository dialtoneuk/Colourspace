<!DOCTYPE html>
<html>
    <?php Flight::render("components/head")?>
    <body>
        <?php Flight::render("components/navbar")?>

        <div class="jumbotron jumbotron-fluid bg-warning text-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="display-4">Shop</h1>
                        <p class="lead">Your changes might take a second to load!</p>
                    </div>
                    <div class="col-sm-4">
                        <p>
                            Max Upload Length <span class="badge badge-light"><?=$model->profiles->userpermissions->uploadmaxlength?></span>
                        </p>
                        <p>
                            Max Upload Size <span class="badge badge-light"><?=$model->profiles->userpermissions->uploadmaxsize?></span>
                        </p>
                        <p>
                            Colour <span class="badge badge-info" style="background-color: rgb(<?=$model->profiles->user->colour?>)"><?=$model->profiles->user->colour?></span>
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <mail role="main">
            <!--Info-->
            <div class="container">
                <?php Flight::render("components/response") ?>
                <!--Featured-->
                <div class="row">
                    <div class="col-sm-12">
                        <h3>
                            Items
                            <small class="text-muted">Upgrades for your account!</small><small class="float-right"><a href="/economics">Previous purchases</a></small>
                        </h3>
                    </div>
                </div>
                <?php Flight::render("components/page_selector.php")?>
                <?php

                    $array = json_decode( json_encode( $model->items ) );

                    foreach( $array as $item )
                    {

                        if( isset( $item->discontinued ) && $item->discontinued == true )
                            continue;


                        Flight::render("components/item", ["item" => $item ] );
                    }
                ?>
            </div>
        </mail>
    </body>
    <?php Flight::render("components/footer")?>
</html>