<!DOCTYPE html>
<html>
<?php Flight::render("components/head")?>
<body>
<?php Flight::render("components/navbar")?>

<div class="jumbotron jumbotron-fluid bg-success text-white">
    <div class="container">
        <h1 class="display-4">Register</h1>
        <p class="lead">Enter your credentials and get uploading some music.</p>
    </div>
</div>

<main role="main">

    <div class="container">
        <?php Flight::render("components/response") ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="username">Username <?php if( isset( $model->temporary ) ){ ?><small>We've already picked one for you!</small><?php } ?></label>
                                <?php

                                    if( isset( $model->temporary ) )
                                    {

                                        ?>
                                            <input type="text" class="form-control" id="username" disabled  placeholder="<?=$model->temporary?>">
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                            <input type="text" class="form-control" id="username" disabled  placeholder="user00000001">
                                        <?php
                                    }
                                ?>

                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="passsword">Password</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="confirm_password">Confirm Password</label>
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm password">
                            </div>
                            <div class="form-group">
                                <?php
                                if( isset( $model->recaptcha ) )
                                    echo( $model->recaptcha->html );
                                ?>
                            </div>
                            <button type="submit" class="btn btn-primary w-100">Register</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h1 class="display-4">
                                            Why share with us?
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="carousel" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <h5>
                                                        We won't limit your creativity.
                                                    </h5>
                                                    <p>
                                                        Upload as much as you want, when ever you want. We'll be waiting day and night.
                                                    </p>
                                                </div>
                                                <div class="carousel-item">
                                                    <h5>
                                                        Earn credits and spend them in our shop
                                                    </h5>
                                                    <p>
                                                        Your interactions on the site will earn you credits which you can
                                                        then spend in the shop. Its like a video game!
                                                    </p>
                                                </div>
                                                <div class="carousel-item">
                                                    <h5>
                                                        Archiving
                                                    </h5>
                                                    <p>
                                                        Uploads your throwaways and give them a place to live. Clean up some space
                                                        on your hard drive.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p>
                                            We are updating Colourspace and telling you about it on our <a href="blog">blog.</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5>
                                            Capitalism sucks
                                        </h5>
                                        <p>
                                            We also offer unlimited uploads and no price tiering on donations. Donate as much as you want and
                                            recieve all the benefits we offer. Inlcuding uploading in multiple formats such as wav, flac and ogg.
                                            Unlimited file lengths mean you can upload as much music as you want.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
<?php Flight::render("components/footer")?>
</html>