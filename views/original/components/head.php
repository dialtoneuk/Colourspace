<head>

    <?php
        if( isset( $page ) && isset( $page->title ) )
            echo("<title>" . $page->title . "</title>");
        else
            echo("<title>Colourspace</title>");
    ?>

    <?php
        if( isset( $model->response ) && isset( $model->response->url ) )
        {

            ?>
                <script>window.location.href = "<?=$model->response->url?>";</script>
            <?php
        }
    ?>

    <!--Hardcoded-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <!--Autos-->
    <?php

       if( isset( $page ) )
       {

           if( isset( $page->css ) )
               foreach( $page->css as $script )
                   echo("<link rel='stylesheet' href=$script>");

           if( isset( $page->header ) )
               foreach( $page->header as $script )
                   echo("<script src=$script></script>");
       }
    ?>
</head>