<?php
    if( isset( $item ) == false )
        throw new Error("Needs item");
?>

<div class="row" style="margin-top: 12px;">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h2><?=$item->name?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <p>
                            <?=$item->description?>
                        </p>
                        <?php
                            if( isset( $item->purchased ) && $item->purchased && $item->onetime )
                            {

                                ?>
                                    <div class="alert alert-warning">
                                        This item is a one time buy, and you've already bought it.
                                    </div>
                                <?php
                            }
                            else
                            {

                                ?>
                                    <form method="post">
                                        <input type="hidden" name="item" value="<?=$item->item?>">

                                        <?php

                                        if( isset( $item->requirements ) && empty( $item->requirements ) == false )
                                        {

                                            $array = json_decode( json_encode( $item->requirements ), true );

                                            foreach( $array as $name=>$type )
                                            {

                                                if( $type == "colour" )
                                                {
                                                    ?>
                                                    <div class="alert alert-info">
                                                        Colours for now are pretty fiddly to enter. Just simply find your rgb values,
                                                        seperated by a comma. An example is as follows:
                                                        <small>255,255,0</small>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="form-group">
                                                    <label class="small" for="<?=$name?>"><?=$name?></label>
                                                    <input type="text" class="form-control" name="<?=$name?>" id="form-<?=$name?>">
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <div class="form-group">
                                            <input class="btn btn-info w-100" type="submit" value="Buy">
                                        </div>
                                    </form>
                                <?php
                            }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            if( isset( $item->purchased ) && $item->purchased && $item->onetime )
                                echo("<div class=\"card bg-warning text-white\">");
                            else
                                echo("<div class=\"card bg-info text-white\">");
                        ?>
                            <div class="card-body">
                                <h1 class="display-4">
                                    <?=number_format( $item->cost )?>
                                    <small style="font-size: 40%;">Waves</small>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>