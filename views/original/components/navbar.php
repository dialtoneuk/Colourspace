<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Colourspace</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <?php
                    if( $model->profiles->session->loggedin )
                        echo('<a class="btn btn-outline-info" href="/upload">Upload</a>');
                ?>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Tracks
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php
                        if( $model->profiles->session->loggedin )
                        {
                            echo('<a class="dropdown-item" href="/profile/' . $model->profiles->user->username . '/tracks">Your Collection</a>');
                            echo('<div class="dropdown-divider"></div>');
                        }
                    ?>
                    <a class="dropdown-item" href="#">Search</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Newest</a>
                    <a class="dropdown-item" href="#">Freshest</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Featured</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Colours
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php
                        if( $model->profiles->session->loggedin )
                        {
                            echo('<a class="dropdown-item" href="/profile/' . $model->profiles->user->username . '">Your profile</a>');
                            echo('<div class="dropdown-divider"></div>');
                        }
                    ?>
                    <a class="dropdown-item" href="#">Search</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Rising</a>
                    <a class="dropdown-item" href="#">Top</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Featured</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <?php
                if( $model->profiles->session->loggedin )
                {

                    if( isset( $model->profiles->balance->amount ) )
                        echo('<a class="nav-link my-2 my-sm-0 text-black-50 font-weight-bold" href="/economics">' . $model->profiles->balance->amount . ' Waves</a>')

                    ?>
                        <a class="btn btn-outline-warning my-2 my-sm-0" href="/shop">Shop</a>
                        <a class="btn btn-outline-danger my-2 my-sm-0" style="margin-left: 12px;" href="/logout">Logout</a>
                        <a class="btn btn-outline-success my-2 my-sm-0" style="margin-left: 12px;" href="/account"><?=$model->profiles->user->username?></a>
                    <?php
                }
                else
                {

                    ?>
                        <a class="btn btn-outline-warning my-2 my-sm-0" href="login">Login</a>
                        <a class="btn btn-outline-warning my-2 my-sm-0" style="margin-left: 12px;" href="register">Register</a>
                    <?php
                }
            ?>
        </form>
    </div>
</nav>
