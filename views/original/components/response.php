<?php

$errors = [
    FORM_ERROR_GENERAL => "alert-danger",
    FORM_ERROR_INCORRECT => "alert-warning",
    FORM_ERROR_MISSING => "alert-danger"
];

$messages = [
    FORM_MESSAGE_SUCCESS => "alert-success",
    FORM_MESSAGE_INFO => "alert-info"
];

if( isset( $model->response ) )
{

    if( isset( $response ) == false )
        $response = json_decode( json_encode( $model->response ), true );

    if( isset( $response["type"] ) == false )
        return;

    if( isset( $errors[ $response["type"] ]) )
    {

        ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert <?=$errors[ $response["type"] ]?>" role="alert">
                        <?=$response["message"]?>
                    </div>
                </div>
            </div>
        <?php
    }
    elseif( isset( $messages[ $response["type"] ] )  )
    {
        ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert <?=$messages[ $response["type"] ]?>" role="alert">
                        <?=$response["message"]?>
                    </div>
                </div>
            </div>
        <?php
    }
    else
    {
        ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        <?=$response["value"]?>
                    </div>
                </div>
            </div>
        <?php
    }
}