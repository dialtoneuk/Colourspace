<div class="row">
    <?php
    if( isset( $model->page ) && isset( $model->page->current_page ) && isset( $model->page->total_pages ) )
    {

        ?>
        <div class="col-sm-12">
            <?php

            if( $model->page->current_page == 0 )
            {

                echo("<a href=?page=1>Next</a> ");
                echo("<span class='text-muted'>Previous</span> ");
            }
            else
            {

                if( $model->page->current_page < $model->page->total_pages - 1 )
                    echo("<a href=?page=" . ( $model->page->current_page + 1 ) . ">Next</a> ");
                else
                    echo("<span class='text-muted'>Next</span> ");

                echo("<a href=?page=" . ( $model->page->current_page  - 1  ). ">Previous</a> ");
            }
            ?>
            <small class="text-muted float-right"> current page <?=$model->page->current_page?> | <?=$model->page->total_pages?> pages in total </small>
        </div>
        <?php
    }
    ?>
</div>