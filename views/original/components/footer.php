<footer class="container" style="margin-top: 12px;">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <?php
                $url = $_SERVER['REQUEST_URI'];

                if( empty( $url ) || $url == "/")
                    echo("<li class=\"breadcrumb-item active\" aria-current=\"page\">Home</li>");
                else
                {

                    $get = explode("?", $url );

                    if( empty( $get ) == false )
                        $url = $get[0];

                    $exploded = explode("/", $url );

                    if( empty( $exploded ) )
                    {

                        echo("><a href='/'>Home</a></li>");
                        echo("<li class=\"breadcrumb-item active text-capitalize\"' aria-current=\"page\">$url</li>");
                    }
                    else
                    {

                        echo("<li class=\"breadcrumb-item \" aria-current=\"page\"><a href='/'>Home</a></li>");

                        $stem = "";

                        foreach( $exploded as $url )
                        {

                            $url = htmlspecialchars( $url );

                            if( empty( $url ) )
                                continue;

                            $stem = $stem . $url;

                            if( end( $exploded ) == $url )
                                echo("<li class=\"breadcrumb-item active text-capitalize\"' aria-current=\"page\">$url</li>");
                            else
                                echo("<li class=\"breadcrumb-item \"><a href=$stem>$url</a></li>");
                        }
                    }
                }
            ?>
        </ol>
    </nav>
    <div class="row" style="padding-bottom: 12px;">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <span style="color: red" class="font-weight-bold text-uppercase"><?=COLOURSPACE_VERSION_PHASE . " " . COLOURSPACE_VERSION_NUMBER?></span> <small>Land Of The Flower People &copy; 2018. Written in PHP. <a href="">Github Repo</a>. Coding by Lewis Lancaster &copy; 2018</small>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php
if( isset( $page ) && empty( $page->footer ) == false )
    foreach( $page->footer as $script )
        echo( "<script type='text/javascript' src='$script'></script>")
?>