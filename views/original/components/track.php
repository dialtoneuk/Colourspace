<?php
    if( isset( $track ) == false )
        throw new Error("Track not set");
?>

<div class="row" style="padding-top: 12px;" >
    <div class="col-sm-12">
        <div class="card text-white" style="background-color: rgb(<?=$track->colour?>); border: 1px solid #212529; border-bottom-left-radius: 0; border-bottom-right-radius: 0;">
            <img class="card-img" src="/<?=$track->metainfo->waveform?>" style="height: 164px; opacity: 0.5;" alt="">
            <div class="card-img-overlay">
                <h5 class="card-title"><?=$track->trackname?> <small>by <?=$track->metainfo->aka?></small></h5>
                <audio class="component" controls>
                    <?php
                        if( isset( $track->streams->mp3 ) )
                            echo('<source src="/' . $track->streams->mp3 . '" type="audio/mpeg">');

                        if( isset( $track->streams->wav ) )
                            echo('<source src="/' . $track->streams->wav . '" type="audio/wav">');

                        if( isset( $track->streams->flac) )
                            echo('<source src="/' . $track->streams->flac . '" type="audio/flac">');
                    ?>
                </audio>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                        <div class="col-sm-8">
                            <h5>Description</h5>
                            <?=$track->metainfo->description?>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-warning w-100">Edit</button>
                            <button class="btn btn-warning w-100" style="margin-top: 12px;">Delete</button>
                            <button class="btn btn-warning w-100" style="margin-top: 12px;">Share</button>
                        </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <small>Uploaded <?=$track->creation?> / rgb(<?=$track->colour?>) / Presented by Colourspace</small>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


