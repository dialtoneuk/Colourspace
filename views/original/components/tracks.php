<?php
if( isset( $model->tracks ) == false )
    return null;

if( $model->profiles->session->loggedin )
    foreach( $model->tracks as $track )
        Flight::render("components/track.php", array( "track" => $track, "description" => true ) );
?>