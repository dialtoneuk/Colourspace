<!DOCTYPE html>
<html>
    <?php Flight::render("components/head")?>
    <body>
        <?php Flight::render("components/navbar")?>

        <div class="jumbotron jumbotron-fluid bg-primary text-white">
            <div class="container">
                <h1 class="display-4">Login</h1>
                <p class="lead">Enter your credentials and get uploading some music.</p>
            </div>
        </div>

        <main role="main">

            <div class="container">
                <?php Flight::render("components/response") ?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="passsword">Password</label>
                                        <input type="password" class="form-control" name="password" id="passsword" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <?php
                                            if( isset( $model->recaptcha ) )
                                                echo( $model->recaptcha->html );
                                        ?>
                                    </div>
                                    <button type="submit" class="btn btn-primary w-100">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h1 class="display-4">
                                                    Why share with us?
                                                </h1>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="carousel" class="carousel slide" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="carousel-item active">
                                                            <h5>
                                                                We won't limit your creativity.
                                                            </h5>
                                                            <p>
                                                                Upload as much as you want, when ever you want. We'll be waiting day and night.
                                                            </p>
                                                        </div>
                                                        <div class="carousel-item">
                                                            <h5>
                                                                We won't ever give you up.
                                                            </h5>
                                                            <p>
                                                                Your tracks are safe with us until the end of time. Even if we go defunct.
                                                            </p>
                                                        </div>
                                                        <div class="carousel-item">
                                                            <h5>
                                                                We won't ever let you down.
                                                            </h5>
                                                            <p>
                                                                We understand how being a musician feels, as we are ones. We won't let you down brother.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>
                                                    We are updating Colourspace and telling you about it on our <a href="blog">blog.</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 12px;">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <a class="btn btn-success w-100" href="register" role="button">Register</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
    <?php Flight::render("components/footer")?>
</html>