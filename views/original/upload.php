<!DOCTYPE html>
<html>
    <?php Flight::render("components/head")?>
    <body>
        <?php Flight::render("components/navbar")?>
        <form method="post" enctype="multipart/form-data">
            <div class="jumbotron jumbotron-fluid bg-success text-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h1 class="display-4">Upload</h1>
                        <p class="lead">Time to show somebody what you've made.</p>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <input type="file" class="form-control w-100" style="height: 132px;" name="<?=UPLOADS_POST_KEY?>" accept=".mp3,.wav,.flac,.ogg">
                        </div>
                    </div>
                </div>

            </div>
        </div>
            <div class="container">
                <?php Flight::render("components/response")?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="text-center">Playtime Limit</h4>
                                <div class=" alert alert-warning text-center">
                                    <?=$model->profiles->userpermissions->uploadmaxlength?> seconds
                                    <p class="small">your max applicable runtime in seconds</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="text-center">Filesize Limit</h4>
                                <div class=" alert alert-info text-center">
                                    <?php
                                        if( $model->profiles->userpermissions->uploadmaxsize == -1 )
                                        {

                                            echo( UPLOADS_MAX_SIZE_GLOBAL . "mb" )

                                            ?>
                                                <p class="small">which is the max we will allow you to upload</p>
                                            <?php
                                        }
                                        else
                                        {

                                            echo( $model->profiles->userpermissions->uploadmaxsize . "mb" );

                                            ?>
                                                <p class="small">You can buy more at the <a href="/shop">shop!</a></p>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="text-center">Lossless</h4>
                                <?php
                                    if( isset( $model->profiles->userpermissions->lossless ) || $model->profiles->userpermissions->lossless == true )
                                    {

                                        ?>
                                            <div class="alert alert-success text-center">
                                                Yes!
                                                <p class="small">You can upload in .flac, .wav, .ogg and .mp3</p>
                                            </div>
                                        <?php
                                    }
                                    else
                                    {

                                        ?>
                                            <div class="alert alert-danger text-center">
                                                No!
                                            </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-sm-12">
                        <h3>
                            Details
                            <small class="text-muted">Tell us, what is it you are uploading? and how do you want it to be precieved.</small>
                        </h3>
                    </div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="alert alert-info">
                                            Markdown formatted is supported for track descriptions. As well as image hotlinking!
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="my fresh hot track">
                                        </div>
                                        <div class="form-group">
                                            <label for="privacy">Privacy</label>
                                            <select class="form-control" name="privacy" id="privacy">
                                                <option value="public" selected >Public</option>
                                                <option value="private">Private</option>
                                                <option value="personal" >Personal</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                                if( isset( $model->recaptcha ) )
                                                    echo( $model->recaptcha->html );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6" >
                        <div class="form-group">
                            <textarea class="form-control" name="description" id="description" style="min-height: 305px; max-height: 305px;" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary w-100">Upload</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
    <?php Flight::render("components/footer")?>
</html>