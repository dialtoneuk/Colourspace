<!DOCTYPE html>
<html>
    <?php Flight::render("components/head")?>
    <body>

        <?php Flight::render("components/navbar")?>

        <div class="jumbotron jumbotron-fluid bg-danger text-white">
            <div class="container">
                <h1 class="display-4">Colourspace</h1>
                <p class="lead">Unlimited music sharing for antisocial musicians.</p>
            </div>
        </div>
        <main role="main">
            <!--Info-->
            <div class="container">
                <?php Flight::render("components/response") ?>
                <!--Info-->
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="display-2">What is your favourite colour?</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h2><i class="fas fa-barcode"></i> Unlimited Uploads</h2>
                        <p>Want to dump 1,000 unreleased tracks?</p>
                    </div>
                    <div class="col-md-4">
                        <h2><i class="fas fa-broadcast-tower"></i> Anoynmous Posting</h2>
                        <p>Want to just post music?</p>
                    </div>
                    <div class="col-md-4">
                        <h2><i class="fas fa-book-open"></i> Archiving</h2>
                        <p>Want all of your stuff to stay backed up?</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">For Musicians</h5>
                                <p class="card-text">Being musicians our selves. We've spent a lot of time debating what mix of systems will and
                                wont make a great platform to share our work on. We are constantly updating and improving. Our code is also <a href="">open source!</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Originality</h5>
                                <p class="card-text">We heavily promote creative intentions with this platform and we have designed the platform to be
                                    well fitted for musicians and other forms of sound artists. We will only host original compositions by our users.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Funoconomy</h5>
                                <p class="card-text">Earn account upgrades through payouts which are given everytime you contribute or do anything on the
                                platform. Buy upgrades such as the ability to upload in what ever format you like, track length restriction upgrades and more!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <button class="btn btn-lg btn-outline-info w-100" href="">Join our Discord</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <button class="btn btn-lg btn-outline-info w-100" href="">View our Bandcamp</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Featured-->
                <div class="row" style="margin-top: 12px;">
                    <div class="col-sm-12">
                        <h3>
                            Featured Tracks
                            <small class="text-muted">Hand selected by us.</small>
                        </h3>
                    </div>
                </div>
            </div>
            <!--Featured Tracks-->
            <div class="container">

            </div>
        </main>
    </body>
    <?php Flight::render("components/footer")?>
</html>