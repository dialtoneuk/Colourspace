<!DOCTYPE html>
<html>
    <?php Flight::render("components/head")?>
    <body>
        <?php Flight::render("components/navbar")?>

        <div class="jumbotron jumbotron-fluid text-white" style="background-color: rgb(<?=$model->profiles->user->colour?>)">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h1 class="display-4"><?=$model->profiles->user->username?></h1>
                        <p class="lead">Joined <?=$model->profiles->user->creation?></p>

                        <?php
                            if( isset( $model->profiles->group ) )
                            {
                                ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body text-black-50">
                                                    <?=$model->profiles->group->name?> <small><?=$model->profiles->group->description?> </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <button class="btn w-100 btn-outline-dark">Profile</button>
                        <button class="btn w-100 btn-outline-dark" style="margin-top: 12px;">Transactions</button>
                        <button class="btn w-100 btn-outline-dark" style="margin-top: 12px;">Stats</button>
                        <button class="btn w-100 btn-outline-dark" style="margin-top: 12px;">Delete Account</button>
                    </div>
                </div>

            </div>
        </div>

        <main role="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>
                            User Permissions
                            <small class="text-muted">Temporary dump of what you can and cant do.</small>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
<pre>
<?=print_r($model->profiles->userpermissions)?>
</pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-sm-12">
                        <h3>
                            Account Settings
                            <small class="text-muted">Edit your account details.</small>
                        </h3>
                    </div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <form method="post">
                                            <input type="hidden" value="password" name="action">
                                            <div class="form-group">
                                                <label for="password">Current Password</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <label for="password">New Password</label>
                                                <input type="password" class="form-control" id="new-password" name="new-password" placeholder="New Password">
                                            </div>
                                            <input type="submit" class="btn btn-primary w-100" value="Change Password">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <form method="post">
                                            <input type="hidden" value="email" name="action">
                                            <div class="form-group">
                                                <label for="password">Current Password</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">New Email address</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                                            </div>
                                            <input type="submit" class="btn btn-primary w-100" value="Change Email">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
    <?php Flight::render("components/footer")?>
</html>