<!DOCTYPE html>
<html>
    <?php Flight::render("components/head")?>
    <body>
        <?php Flight::render("components/navbar")?>

        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Verification</h1>
                <p class="lead">Please enter your key or mobile authentication code.</p>
            </div>
        </div>

        <main role="main">

            <div class="container">
                <?php Flight::render("components/response") ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <form method="post">
                                    <div class="row">
                                        <div class="col">
                                            <input type="text" class="form-control" name="key" placeholder="ZnVja3dpbGx5YnVtcw==">
                                        </div>
                                        <div class="col">
                                            <select class="form-control" name="type">
                                                <option value="email" selected>Email</option>
                                                <option value="mobile">Mobile</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                            if( isset( $model->recaptcha ) )
                                                echo( $model->recaptcha->html );
                                        ?>
                                    </div>
                                    <button type="submit" class="btn btn-success w-100">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
    <?php Flight::render("components/footer")?>
</html>