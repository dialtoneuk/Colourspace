## Colourspace

Music sharing platform written in PHP 7 powered by Flights micro php framework. Featuring an
economy system and direct intergration with a cloud storage solution of your choice.

### Installation

Please download a compiled release from the releases section. Colourspace can also be ran via composer but needs to be
invoked as it is meant for development purposes. Place the downloade release in your htdocks or www folder and then
login to your webserver or start a new temrinal instance. If you would like to set up Colourspace via composer. You will
need to copy over all the folder, except for the src and vendor folders into your webservers root directory.

#### Console Interface

Colourspace can be configured using a series of console commands which can be accessed using
a command line interface.

To start a command line interface instance, you can use your IDE of choice, or create a terminal
window with the same root as Colourspace. Once you have initiated an instance using the method
of your choice. You will be able to easily edit, configure and maintain colourspace through the
console. 

( Note: Scripts given in the script argument are not case sensitive ).

##### Getting started

Scripts are executed by invoking the execute php script inside the cmd folder within Colourspaces root. It
is important that you make sure to execute the php script from the same root as Colourspace. To execute
the help script, run the following command into your terminal window.

`php -f cmd/execute.php help script=`

The syntax for the console command is as follows. The `-f` modifier turns all inputs after this
modifier into a key value array which is then invoked inside PHP under the name of `$argv`. Scripts
will fail to execute correctly if you have not put this modifier before you reference the php script.

The second argument is the script of which to execute. In this instance, it is help. Any arguments
after the first two are counted as script arguments which are parsed and passed to the script which
you are executing. The syntax for this is explained further more inside the help script. Here is a functional
example of how the arguments work.

`php -f cmd/execute.php autoexec script=register arguments=userid:1,message:AUTO`

The left side of the equals is the name of the argument, the key. The right side of the equals is the value. In
this special case. AutoExec takes a nested array as one of its arguments. These are essentially children
arrays which allow you to pass an array instead of a singular value. This is a special case and only
used in this instance.

A good start would be to execute the setup script. Colourspace automatically comes with an installation
tool which will guide you through all the configuration options required. You can execute this script
by invoking the following command into your terminal window.

`php -f cmd/execute.php setup setup=`

If you would like to set up a specific component, then the set up argument can be switched to the name
of the set up script you would like to execute. You can find the names of the set up scripts by looking
inside the `src/Framework/Util/Setups/` folder and then using the lower case filename with out the file
extension. An example of this is as follows.

`php -f cmd/execute.php setup setup=database`

Remember that all scripts on failure will result in error code 0. Be sure to enter the correct
value types for your arguments when working with the command line interface. Remember to put **.php** at the
end of `cmd/execute`.

##### Deploying a Database Connection Dynamically

As previously explained, through the command line interface it is possible to configure Colourspace to your
current hosting solution. Be it a dynamic solution or a static solution. Colourspace reads its
database connection settings from an internal json configuration file. This file can be wrote
to via the command line interface. You would use this in conjunction with a bash script on a dynamic
system to automatically invoke the execute script to propogate your connection settings. Here is an example
of how you can do it.

`php -f cmd/execute.php deploy username=root password= database=colourspace host=localhost driver=mysql charset=utf8 collation=utf8_unicode_ci prefix=`

This only needs to be executed once when the server is deployed and your connection settings will be
saved and loaded each time Colourspace recieves a request.

##### Migrating

We are going to assume that you've set up your configuration details. Before you run the full test. You
will need to migrate your database ready for Colourspace. We are going to assume you have set up a
MySQL database for the sake of explaining, but this will work with any database platform you are using. To
migrate, simply excute the following command into your terminal.

`php -f cmd/execute.php automigrate`

This will create the tables inside your database needed for Colourspace to function. As well as migrate
some json files which need their times updated.

##### Testing

Testing is automatic and can be invokved by a console command. Colourspace comes with a series of predefined
tests which will assure you have configured your configuration files correct. It does not how ever test
upload functionality as of now. Here is how you execute it.

`php.exe -f cmd/execute.php autotest`

The script will either return error code 0 on success, or error code 1 on fail. This will work with your
automatic testing solutions if you are building and working with Colourspace.

##### Resources

Colourspaces packs its json files into a resources data blob in order to protect against accidential
public leaks of information such as database passwords, API keys, and other things. As well as that too, it
also allows PHP to "write its own files" which eleviates various common permission errors. If you are using
git to work with Colourspace. Your .gitignore is naturally attuned to ignore anything inside config
which isn't the resources blob file. **Before you push to git, you need to run the resources script and
pack your resources.** To do this, simply execute the console command below into your terminal.

`php -f cmd/execute.php resources action=pack`

### Status

Colourspace is a **work in progress** and is not finished. Please check out our road map and current
release schedule for where we are at in the journey of making the best music sharing platform ever.

### Modifying & Contributions

For guides on how to mod Colourspace. Please check out the wiki for indepth programming tutorials. If you
would like to contribute to the development of Colourspace. Please check out our policy. The only rule
we like to enforce is code standards. Please keep your code clean and efficient, and in line with
the rest of the application.

### Credits

Colourspace was made possible using various underlying libraries and technologies. A full comprehensive
list will be written in the future.

 * Lewis Lancaster (Programming)
 * Twisty Blackwood (Support & Ideas)
 * Jugboy (Support & Ideas)
 * Azzy (Support)
 
 Special thanks to
 
 Jecosity, Mak Attack. Kracken and Ellistron.