<?php
require_once( "vendor/autoload.php" );

//<editor-fold defaultstate="collapsed" desc="Namespaces">

use Colourspace\Application;
use Colourspace\Database\Connection;
use Colourspace\Container;
use Colourspace\Framework\FrontController;
use Colourspace\Framework\Session;
use Colourspace\Framework\Router;
use Colourspace\Framework\Util\Debug;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Interfaces\ReturnsInterface;

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="PHP pre checks">

/**
 * PHP pre checks
 * =======================================
 */

if (empty( $_SERVER["DOCUMENT_ROOT"] ) )
    $root = getcwd();
else
    $root = $_SERVER["DOCUMENT_ROOT"];

if( substr( $root, -1 ) !== DIRECTORY_SEPARATOR )
    $root = $root . DIRECTORY_SEPARATOR;

if( version_compare(PHP_VERSION, '7.0.0') == -1 )
    die('Please upgrade to PHP 7.0.0+ to run this web application. Your current PHP version is ' . PHP_VERSION );

if( php_sapi_name() === 'cli' && Debug::isCMD() == false )
    die('Please run this web application through your web browser. It wont work via the console! (Muh live programs)');

/**
 * Written by Lewis 'mkultra2018' Lancaster
 * in 2018 (June to August)
 * =======================================
 */

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Application Settings">

/**
 * Application Settings
 * =======================================
 */

//Colourspace

define("COLOURSPACE_ROOT", $root );
define("COLOURSPACE_URL_ROOT", "/");
define("COLOURSPACE_NAMESPACE_ROOT", "Colourspace\\");
define("COLOURSPACE_URL_ADDRESS", "http://localhost");
define("COLOURSPACE_VERSION_PHASE","alpha");
define("COLOURSPACE_VERSION_NUMBER","0.1.5");

//Framework
define("FRAMEWORK_BASECLASS", "base");  //This ties to a few of the instance builders and won't build a direct instance of any class called "Base". This is useful in the
                                        //Util namespace for not directly invoking my interfaces and leaving them easily modifiable. If you want to load the base, even
                                        //though it could potentially crash the system. Change "base" to "".
//Website
define("WEBSITE_TITLE", "Colourspace");
define("WEBSITE_JQUERY", "jquery-3.3.1.min.js");
define("WEBSITE_BOOTSTRAP4", "bootstrap.js");

//User Accounts
define("ACCOUNT_PREFIX", "user");
define("ACCOUNT_DIGITS", 8);
define("ACCOUNT_RND_MIN", 1);
define("ACCOUNT_RND_MAX", 8);
define("ACCOUNT_PASSWORD_MIN", 8);
define("ACCOUNT_PASSWORD_STRICT", false );

//Tracks
define("TRACK_PRIVACY_PUBLIC", "public");
define("TRACK_PRIVACY_PRIVATE", "private");
define("TRACK_PRIVACY_PERSONAL", "personal");
define("TRACK_PREFIX", "track");
define("TRACK_NAME_MAXLENGTH", 64);
define("TRACK_DIGITS", 12);
define("TRACK_RND_MIN", 0);
define("TRACK_RND_MAX", 9);

//Global Upload Settings
define("UPLOADS_TEMPORARY_DIRECTORY", "files/temp/");
define("UPLOADS_FILEPATH","src/Framework/Uploads/");
define("UPLOADS_NAMESPACE","Colourspace\\Framework\\Uploads\\");
define("UPLOADS_LOCAL", true ); //Will keep files in the temporary directory instead of uploading them ( should only be used for testing )
define("UPLOADS_WAVEFORMS_LOCAL", true ); //Will keep wave forms in the temporary directory instead of uploading them. Use with global above to completely turn off the data handler.
define("UPLOADS_POST_KEY", "track");
define("UPLOADS_MAX_SIZE_GLOBAL", 500); //Used to define the max applicable size anybody can upload
define("UPLOADS_ERROR_NOT_FOUND", 1 );
define("UPLOADS_ERROR_FILENAME", 2 );
define("UPLOADS_ERROR_EXTENSION", 3 );
define("UPLOADS_ERROR_TOO_LARGE", 4 );
define("UPLOADS_ERROR_CANCELLED", 5 );


define("SCRIPTS_ROOT","src/Framework/Util/Scripts/");
define("SCRIPTS_NAMESPACE", "Colourspace\\Framework\\Util\\Scripts\\");
define("SCRIPTS_REQUIRE_CMD", true );

//ffmeg
define("FFMPEG_CONFIG_FILE","config/framework/ffmpeg.json");

//Verification
define("VERIFICATIONS_NAMESPACE", "Colourspace\\Framework\\Verifications\\");
define("VERIFICATIONS_ROOT", "src/Framework/Verifications/");
define("VERIFICATIONS_TYPE_EMAIL", "email");
define("VERIFICATIONS_TYPE_MOBILE", "mobile");

//Amazon
define("AMAZON_BUCKET_URL", "https://s3.eu-west-2.amazonaws.com/colourspace/");
define("AMAZON_CREDENTIALS_FILE", "config/storage/amazon.json");
define("AMAZON_S3_BUCKET", "colourspace");
define("AMAZON_LOCATION_US_WEST", "us-west-1");
define("AMAZON_LOCATION_US_WEST_2", "us-west-2");
define("AMAZON_LOCATION_US_EAST", "us-east-1");
define("AMAZON_LOCATION_US_EAST_2", "us-east-2");
define("AMAZON_LOCATION_CA_CENTRAL", "ca-central-1");
define("AMAZON_LOCATION_EU_WEST", "eu-west-1");
define("AMAZON_LOCATION_EU_WEST_2", "eu-west-2");
define("AMAZON_LOCATION_EU_CENTRAL", "eu-central-1");

//Google recaptcha
define("GOOGLE_RECAPTCHA_ENABLED", false );
define("GOOGLE_RECAPTCHA_CREDENTIALS", "config/framework/google_recaptcha.json" );

//Google Cloud Storage
define("GOOGLE_CLOUD_CREDENTIALS",  "config/storage/google.json");

//Cloud Storage
define("STORAGE_CONFIG_ROOT","config/storage/");
define("STORAGE_SETTINGS_FILE","settings.json");

//Flight
define("FLIGHT_JQUERY_FILE", "jquery-3.3.1.min.js");
define("FLIGHT_CONTENT_OBJECT", true ); //Instead, convert $model into an object ( which is cleaner )
define("FLIGHT_MODEL_DEFINITION", "model" );
define("FLIGHT_PAGE_DEFINITION", "page" );
define("FLIGHT_SET_GLOBALS", true );
define("FLIGHT_VIEW_FOLDER", "views/original/" );

///Twig
define("TWIG_VIEW_FOLDER", "views/rework/");

//Setups
define("SETUP_ROOT", "src/Framework/Util/Setups/");
define("SETUP_NAMESPACE", "Colourspace\\Framework\\Util\\Setups\\");

//MVC
define("MVC_NAMESPACE", "Colourspace\\Framework\\");
define("MVC_NAMESPACE_MODELS", "Models");
define("MVC_NAMESPACE_VIEWS", "Views");
define("MVC_NAMESPACE_CONTROLLERS", "Controllers");
define("MVC_TYPE_MODEL", "model");
define("MVC_TYPE_VIEW", "view");
define("MVC_TYPE_CONTROLLER", "controller");
define("MVC_REQUEST_POST", "POST");
define("MVC_REQUEST_GET", "GET");
define("MVC_REQUEST_PUT", "PUT");
define("MVC_REQUEST_DELETE", "DELETE");
define('MVC_ROUTE_FILE', 'config/routes.json');
define("MVC_ROOT", "src/Framework/");

//Pages
define("PAGE_SIZE", 6 ); //Default page size: 6 objects wide

//Form
define("FORM_ERROR_GENERAL", "general_error");
define("FORM_ERROR_INCORRECT", "incorrect_information");
define("FORM_ERROR_MISSING", "missing_information");
define("FORM_MESSAGE_SUCCESS", "success_message");
define("FORM_MESSAGE_INFO", "info_message");
define("FORM_DATA", "data");

//Resource combiner

define("RESOURCE_COMBINER_ROOT", "config/");
define("RESOURCE_COMBINER_CHMOD", true );
define("RESOURCE_COMBINER_CHMOD_PERM", 0755 );
define("RESOURCE_COMBINER_PRETTY", true );
define("RESOURCE_COMBINER_FILEPATH", "config/resources" );

//Fields
define("FIELD_TYPE_INCREMENTS","increments");
define("FIELD_TYPE_STRING","string");
define("FIELD_TYPE_INT","integer");
define("FIELD_TYPE_PRIMARY","primary");
define("FIELD_TYPE_TIMESTAMP","timestamp");
define("FIELD_TYPE_DECIMAL","decimal");
define("FIELD_TYPE_JSON","json");
define("FIELD_TYPE_IPADDRESS","ipAddress");

//Columns
define("COLUMN_USERID", "userid");
define("COLUMN_SESSIONID", "sessionid");
define("COLUMN_CREATION", "creation");
define("COLUMN_METAINFO", "metainfo");
define("COLUMN_TRACKID", "trackid");

//Tables
define("TABLES_NAMESPACE", "Colourspace\\Database\\Tables\\");
define("TABLES_ROOT", "src/Database/Tables/");

//Tests
define("TESTS_NAMESPACE", "Colourspace\\Framework\\Util\\Tests\\");
define("TESTS_ROOT", "src/Framework/Util/Tests/");

//Audit (Moderation)
define("AUDIT_TYPE_BAN","ban");
define("AUDIT_TYPE_WARNING","warning");
define("AUDIT_TYPE_GROUPCHANGE","groupchange");

//Log
define("LOG_ROOT","/config/debug/log.json");
define("LOG_TYPE_GENERAL", "general");
define("LOG_TYPE_WARNING", "warning");
define("LOG_TYPE_DEFAULT", "default");

//Auto Execute
define("AUTOEXEC_ROOT", "src/Framework/Util/AutoExecs/");
define("AUTOEXEC_NAMESPACE", "Colourspace\\Framework\\Util\\AutoExecs\\");
define("AUTOEXEC_SCRIPTS_ROOT","config/autoexec/");
define("AUTOEXEC_LOG_REFRESH", 12 ); //In hours
define("AUTOEXEC_LOG_LOCATION","config/debug/log/");

//Database Settings
define("DATABASE_ENCRYPTION", false);
define("DATABSAE_ENCRYPTION_KEY", null ); //Replace null with a string of a key to not use a rand gen key.
define("DATABASE_CREDENTIALS", "config/database/credentials.json");
define("DATABASE_MAP", "config/database/map.json");

//Groups
define("GROUPS_ROOT", "config/groups/");
define("GROUPS_DEFAULT", "default");
define("GROUPS_FLAG_MAXLENGTH", "uploadmaxlength");
define("GROUPS_FLAG_MAXSIZE", "uploadmaxsize");
define("GROUPS_FLAG_LOSSLESS", "lossless");
define("GROUPS_FLAG_ADMIN", "admin");
define("GROUPS_FLAG_DEVELOPER", "developer");

//User Permissions
define("USER_PERMISSIONS_ROOT", "config/groups/user/");


//Featured
define("FEATURED_ROOT", "config/featured/");
define("FEATURED_ARTISTS", "artists");
define("FEATURED_TRACKS", "tracks");

//Stream audio codec types
define("STREAMS_MP3", "mp3");
define("STREAMS_FLAC", "flac");
define("STREAMS_OGG", "ogg");
define("STREAMS_WAV", "wav");

//Debugging Options
define("DEBUG_ENABLED", true ); //Will write debug messages and echo them inside the terminal instance
define("DEBUG_WRITE_FILE", true );
define("DEBUG_MESSAGES_FILE", 'config/debug/messages.json');
define("DEBUG_TIMERS_FILE", 'config/debug/timers.json');

//Mailer
define("MAILER_CONFIGURATION_FILE", "config/framework/templates.json");
define("MAILER_TEMPLATES_ROOT", "config/templates/");
define("MAILER_IS_HTML", true );
define("MAILER_IS_SMTP", true );
define("MAILER_FROM_ADDRESS", "user00000001@colourspace.io" );
define("MAILER_FROM_USER", "user00000001" );
define("MAILER_CONTACT_ADDRESS", "support@colourspace.io" );
define("MAILER_VERIFY_TEMPLATE", "email" );
define("MAILER_BANNED_TEMPLATE", "banned" );
define("MAILER_REMOVED_TEMPLATE", "removed" );
define("MAILER_POSTED_TEMPLATE", "posted" );
define("MAILER_COMMENTS_TEMPLATE", "comments" );

//Javascript Builder
define("SCRIPT_BUILDER_ENABLED", true ); //It isnt recommended you turn this on unless your compiled.js for some reason is missing or you are developing.
define("SCRIPT_BUILDER_ROOT", "assets/scripts/");
define("SCRIPT_BUILDER_FREQUENCY", 60 * 60 * 2); //Change the last digit for hours. Remove a "* 60" for minutes.
define("SCRIPT_BUILDER_COMPILED", "compiled.js");
define("SCRIPT_BUILDER_FORCED", false ) ;//Compiles a fresh build each request regardless of frequency setting.

//Misc
define("COLLECTOR_DEFAULT_NAMESPACE", "Colourspace\\Framework\\");

//Colours
define("COLOURS_OUTPUT_HEX", 1);
define("COLOURS_OUTPUT_RGB", 2);

//Shop
define("SHOP_ROOT","src/Framework/Items/");
define("SHOP_NAMESPACE","Colourspace\\Framework\\Items\\");
define("SHOP_INVENTORY","config/shop/items.json");

//Balance
define("BALANCE_DEFAULT_AMOUNT", 100 );

//Transactions
define("TRANSACTION_TYPE_WITHDRAW", "withdraw" );
define("TRANSACTION_TYPE_DEPOSIT", "deposit" );

//Migrator Util
define("MIGRATOR_ROOT", "src/Framework/Util/Migrators/");
define("MIGRATOR_NAMESPACE","Colourspace\\Framework\\Util\\Migrators\\");

//</editor-fold>

//<editor-fold desc="Initialization">

/**
 * Colourspace Initialization
 * =======================================
 */

if( Debug::isCMD() == false || ( Debug::isCMD() && Debug::isTest() ) )
{

    try
    {

        /**
         * Debug Timers
         * =======================
         */


        if( DEBUG_ENABLED )
        {

            //This will automatically allow all the debug methods in the application to function
            Debug::initialization();
            Debug::message("Request initiated");
            Debug::setStartTime('application');
        }

        /**
         * Initialization
         * =======================
         */

        $application = new Application();

        //This must be initiated and created first as some core functions require access to a database.
        $application->connection = new Connection( true );

        //We then run a test of the database connection. This simply pulls the database name from the database which will return an error if it fails
        if( $application->connection->test() == false )
            throw new RuntimeException("Failed connection test, check settings");

        //We then initiate the front controller. This loads all of our MVC classes into memory so there are no load times when we process the request. It is all live from memory.
        $application->frontcontroller = new FrontController( true );

        //We then test this by simply querying the stacks of Views, Models and Controllers and checking if any one of them are empty.
        if( $application->frontcontroller->test() == false )
            throw new RuntimeException("Failed to initiate front controller, check your files");

        //The router file is what is then fed into Flight. Our microframework for easy http routing ( read more up on this at flightphp.com ). All this really does is read from a json file
        //a list of arrays with a url of which to match and initiate this perticular set of MVC classes, and the prementioned MVC classes them selves. You can check out the file
        //for how this works.
        $application->router = new Router( true );

        //Same as the previous test, if we are empty. RuntimeException.
        if( $application->router->test() == false )
            throw new RuntimeException('Router failed to initiate, check your router files');

        //We then create the session class, but we do not initialize. Read below for more info on this.
        $application->session = new Session( false );

        //After everything is done, we globalize the application class to be accessible where ever we are in the web-application. Saving on CPU overhead and memory and time
        //when accessing these heavily used components.
        Container::add("application", $application );

        //The collector is basically a global static object which keeps tracks of all the classes we make returning a pre-loaded instance when the
        //it is needed. It must be initialized. This shouldn't REALLY be called before this point anyway.
        Collector::initialize();

        //After the container has been globalized. We can then initiate the session from invoking it directly inside the container. We do it this way because the
        //session class by default creates a table class so it can cross reference with a database to check things such as login states and the current
        //owner of the session. if they are logged in. The table class invokes the connection class to get a current active database connection.
        //So in order to initialize the session class, we first need to globalize the application and along with it our active database connection.
        //You should take this into account when working with the session class and to make sure when working with the database component that the application has been
        //initialized, and added to this global container.
        Container::get('application')->session->initialize();

        /**
         * Flight
         * =======================
         */

        Flight::set('flight.views.path', FLIGHT_VIEW_FOLDER );

        foreach( $application->router->live_routes as $url=>$payload )
        {

            Flight::route( $url, function( ...$arg ) use ( $payload ){

                if( count( $arg ) !== 1 )
                    $route = last( $arg );
                else
                    $route = $arg[0];

                $request = Container::get('application')->frontcontroller->buildRequest( $route );

                $returns = Container::get('application')->frontcontroller->process( $request, $payload );

                if( $returns == false )
                    Flight::notFound();
                else
                {

                    if( $returns instanceof ReturnsInterface == false )
                        throw new RuntimeException("View must return a valid class which implements the return interface");

                    $returns->process();
                }
            }, true );
        }

        Flight::before('start', function(){


            if ( DEBUG_ENABLED )
                Debug::setStartTime('flight_route');
        });

        Flight::after('start', function()
        {
            if( DEBUG_ENABLED  )
            {

                Debug::message("Request Complete");
                Debug::setEndTime('flight_route' );

                if( DEBUG_WRITE_FILE )
                {

                    Debug::stashMessages();
                    Debug::stashTimers();
                }

            }

        });

        Debug::message("Finished application loading");
        Debug::setEndTime('application');

        Flight::start();
    }
    catch ( RuntimeException $error )
    {

        if( Debug::isTest() )
            Debug::echo( "[Critical RuntimeException " . $error->getFile() . " " . $error->getLine() . "] : " . $error->getMessage() . "" );
        else
        {

            print_r( "<pre>" . $error . "</pre>" );
            exit( 1 );
        }
    }
}
else
{
    Debug::echo("-> HEADS UP! <- ");
    Debug::echo( "[ !Application quit because CMD is defined! ]\n");
}

//</editor-fold>
