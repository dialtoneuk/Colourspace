<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 29/08/2018
 * Time: 21:26
 */

namespace Colourspace\Framework\Interfaces;


use Colourspace\Framework\Util\Conventions\UploadData;
use Delight\FileUpload\File;

interface Upload
{


    /**
     * @param UploadData $data
     * @param $userid
     * @return bool
     */

    public function authenticate( UploadData $data, $userid ): bool;

    /**
     * @param UploadData $data
     * @param $userid
     * @return mixed
     */

    public function before( UploadData $data, $userid );

    /**
     * @param File $file
     * @param UploadData $data
     * @param $userid
     * @return void
     */

    public function after( File $file, UploadData $data, $userid );

    /**
     * @param UploadData $data
     * @param $userid
     * @return File|bool
     */

    public function upload( UploadData $data, $userid );
}