<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 14:56
 */
namespace Colourspace\Framework\Interfaces;

/**
 * Interface ModelInterface
 * @var \stdClass
 * @package Colourspace\Framework\Interfaces
 */

interface Model
{

    public function startup();

    public function formMessage($type, $value );

    public function formError($type, $value );

    public function response( array $response );

    public function redirect($url, $delay );

    public function toArray();
}