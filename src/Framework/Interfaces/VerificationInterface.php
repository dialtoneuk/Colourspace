<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 14/07/2018
 * Time: 21:53
 */

namespace Colourspace\Framework\Interfaces;


interface VerificationInterface
{

    /**
     * Should return something
     *
     * @param $data
     * @return mixed
     */

    public function process( $data );

    /**
     * Should verify the token
     *
     * @param $data
     * @return bool
     */

    public function verify( $data );
}