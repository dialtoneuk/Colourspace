<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 18/07/2018
 * Time: 20:52
 */

namespace Colourspace\Framework\Interfaces;


interface Item
{

    /**
     * @param $autocreate
     * @return void
     */

    public function before( $autocreate );

    /**
     * @param $userid
     * @param $data
     * @return mixed
     */

    public function authentication( $userid, $data );

    /**
     * @param $userid
     * @param $data
     * @return mixed
     */


    public function purchase( $userid, $data );
}