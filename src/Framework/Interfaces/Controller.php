<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 14:55
 */
namespace Colourspace\Framework\Interfaces;

use Colourspace\Framework\Util\Interfaces\Response;
use flight\net\Request;

interface Controller
{

    public function setModel(Model $model );

    /**
     * @return array
     */

    public function keyRequirements();

    /**
     * @return mixed
     */

    public function before();

    /**
     * @param string $type
     * @param Request $request
     * @return Response|bool
     */

    public function process( string $type, Request $request );

    /**
     * @param string $type
     * @param Request $request
     * @return mixed
     */

    public function authentication( string $type, Request $request );
}