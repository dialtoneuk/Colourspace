<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 14:57
 */

namespace Colourspace\Framework\Interfaces;


interface ViewInterface
{

    /**
     * @param \Colourspace\Framework\Model $model
     * @return mixed
     */

    public function setModel( \Colourspace\Framework\Model $model );

    /**
     * @param null $template
     * @param null $payload
     * @return mixed
     */

    public function get( $template=null, $payload=null );

    /**
     * @param $type
     * @return mixed
     */

    public function before( $type );
}