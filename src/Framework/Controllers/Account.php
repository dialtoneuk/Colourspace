<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 15:19
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Framework\Controller;
use Colourspace\Container;
use flight\net\Request;

class Account extends Controller
{


    /**
     * @return array
     */

    public function keyRequirements()
    {

        $array = [
            "action",
            "password",
            "new_password",
            "email"
        ];

        if( GOOGLE_RECAPTCHA_ENABLED )
            $array[] = "g-recaptcha-response";

        return( $array );
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     * @throws \RuntimeException
     */

    public function process(string $type, Request $request)
    {

        if( GOOGLE_RECAPTCHA_ENABLED )
            $this->addRecaptcha();

        return true;
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     * @throws \RuntimeException
     */

    public function authentication(string $type, Request $request)
    {

        $application = Container::get('application');

        if( $application->session->isLoggedIn() == false )
            return false;

        return true;
    }
}