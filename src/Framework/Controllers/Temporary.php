<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 06/08/2018
 * Time: 14:41
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Framework\Util\Controller\FormData;
use Colourspace\Framework\Util\Controller\FormError;
use flight\net\Request;

class Temporary extends Register
{

    /**
     * @param string $type
     * @param Request $request
     * @return bool|FormData|FormError|\Colourspace\Framework\Util\Controller\FormRedirect
     * @throws \RuntimeException
     */

    public final function process(string $type, Request $request)
    {

        if( $this->temporaryusername->exist( session_id() ) )
        {

            return new FormData( FORM_DATA, $this->temporaryusername->get( session_id() ) );
        }

        $result = $this->temporaryusername->add( session_id() );

        if( $this->temporaryusername->exist( session_id() ) == false )
            return new FormError(FORM_ERROR_GENERAL, "Failed to create temporary username for session");

        return new FormData( FORM_DATA, $result );
    }

}