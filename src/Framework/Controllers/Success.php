<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 06/08/2018
 * Time: 14:41
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Framework\Util\Controller\FormError;
use Colourspace\Framework\Util\Controller\FormMessage;
use flight\net\Request;

class Success extends Register
{

    /**
     * @param string $type
     * @param Request $request
     * @return bool|FormError|FormMessage|\Colourspace\Framework\Util\Controller\FormRedirect
     * @throws \RuntimeException
     */

    public final function process(string $type, Request $request)
    {

        return new FormMessage( FORM_MESSAGE_SUCCESS, "Success", true );
    }

}