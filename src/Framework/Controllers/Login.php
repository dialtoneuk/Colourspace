<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 15:19
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Framework\Controller;
use Colourspace\Container;
use Colourspace\Framework\Moderation;
use Colourspace\Framework\User;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Util\Controller\FormError;
use Colourspace\Framework\Util\Controller\FormRedirect;
use Colourspace\Framework\Util\Format;
use Colourspace\Framework\Session;
use Colourspace\Framework\Util\Interfaces\Response;
use flight\net\Request;

class Login extends Controller
{

    /**
     * @var User
     */

    protected $user;

    /**
     * @var Session
     */

    protected $session;

    /**
     * @var Moderation
     */

    protected $moderation;


    /**
     * @return array
     */

    public function keyRequirements()
    {

        $array = [
            "email",
            "password"
        ];

        if( GOOGLE_RECAPTCHA_ENABLED )
            $array[] = "g-recaptcha-response";

        return( $array );
    }

    /**
     * @param string $type
     * @param Request $request
     * @return Response|bool
     * @throws \RuntimeException
     * @throws \Exception
     */

    public function process(string $type, Request $request)
    {

        if( GOOGLE_RECAPTCHA_ENABLED )
            $this->addRecaptcha();

        if( $type == MVC_REQUEST_POST )
        {


            if( $this->check( $request->data ) == false )
                return new FormError(FORM_ERROR_MISSING,"Please fill out all the missing fields");
            else
            {

                $form = $this->pickKeys( $request->data, true );

                if( GOOGLE_RECAPTCHA_ENABLED )
                    if( $this->checkRecaptcha( $form ) == false )
                        return new FormError( FORM_ERROR_GENERAL, "Google response invalid");


                $result = $this->checkLogin( $form );

                if( is_array( $result ) )
                    return new FormError($result['type'],$result['value']);
                else
                {

                    if( is_int( $result ) == false )
                        throw new \RuntimeException("Incorrect userid type");

                    $this->session->Login( $result );

                    if( $this->session->isLoggedIn()  == false )
                        throw new \RuntimeException("Failed to login");

                    return new FormRedirect("/");
                }
            }
        }

        return true;
    }

    /**
     * @param $form
     * @return array
     * @throws \RuntimeException
     */

    public function checkLogin( $form )
    {

        if( $this->user->hasEmail( $form->email ) == false )
            return([
                "type" => FORM_ERROR_INCORRECT,
                "value" => "Email invalid, please try again or reset your password"
            ]);

        $user = $this->user->getByEmail( $form->email );

        if( $this->checkPassword( $form->password, $user->password, $user->salt ) == false  )
            return([
                "type" => FORM_ERROR_INCORRECT,
                "value" => "Password invalid, please try again or reset your password"
            ]);

        if( $this->moderation->isBanned( $user->userid ) )
        {

            $ban = $this->moderation->getBanMetainfo( $user->$user );

            return([
                "type" => FORM_ERROR_GENERAL,
                "value" => "You have been banned from colourspace and will be unable to login until " . $ban->expires . ". Reason: " . $ban->reason
            ]);
        }

        return $user->userid;
    }

    /**
     * @throws \RuntimeException
     */

    public function before()
    {

        parent::before();

        $this->user = Collector::new("User");
        $this->session = Container::get('application')->session;
        $this->moderation = Collector::new("Moderation");
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     * @throws \RuntimeException
     */

    public function authentication(string $type, Request $request)
    {

        if( empty( $this->session ) )
            $session = Container::get('application')->session;
        else
            $session = $this->session;

        if( $session->isLoggedIn() )
            return false;

        return true;
    }

    /**
     * @param $given_password
     * @param $encrypted_password
     * @param $salt
     * @return bool
     */

    private function checkPassword( $given_password, $encrypted_password, $salt )
    {

        if( Format::saltedPassword( $salt, $given_password ) == $encrypted_password )
        {

            return true;
        }

        return false;
    }
}