<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 04/07/2018
 * Time: 14:58
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Container;
use Colourspace\Framework\Util\Controller\FormMessage;
use Colourspace\Framework\Util\Controller\FormRedirect;
use flight\net\Request;

class Logout extends Login
{

    /**
     * @param string $type
     * @param Request $request
     * @return bool|FormRedirect|\Colourspace\Framework\Util\Interfaces\Response
     * @throws \RuntimeException
     */

    public function process(string $type, Request $request)
    {

        if( $this->session->isActive() )
            $this->session->destroy( true );

        return new FormMessage( FORM_MESSAGE_INFO, "You have been successfully logged out");
    }

    /**
     * @return array
     */

    public function keyRequirements()
    {

        return ([]);
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     * @throws \RuntimeException
     */

    public function authentication(string $type, Request $request)
    {

        $application = Container::get('application');

        if( $application->session->isLoggedIn() == false )
            return false;

        return true;
    }
}