<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 15:19
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Framework\Controller;
use Colourspace\Container;
use Colourspace\Framework\Util\AutoExec;
use Colourspace\Framework\Util\Controller\FormError;
use Colourspace\Framework\TemporaryUsername;
use Colourspace\Framework\User;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Util\Controller\FormRedirect;
use flight\net\Request;

class Register extends Controller
{

    /**
     * @var User
     */

    protected $user;

    /**
     * @var TemporaryUsername
     */

    protected $temporaryusername;

    /**
     * @var AutoExec
     */

    protected $autoexec;

    /**
     * Annoyingly strict passwords
     *
     * @var array
     */

    protected $password_strict_stems = [
        "@[A-Z]@",
        "@[a-z]@",
        "@[0-9]@"
    ];

    /**
     * @return array
     */

    public function keyRequirements()
    {

        $array = [
            "email",
            "password",
            "confirm_password"
        ];

        if( GOOGLE_RECAPTCHA_ENABLED )
            $array[] = "g-recaptcha-response";

        return( $array );
    }


    /**
     * @throws \RuntimeException
     */

    public function before()
    {

        parent::before();

        $this->user = Collector::new("User");
        $this->autoexec = Collector::new("AutoExec","Colourspace\\Framework\\Util\\");
        $this->temporaryusername = Collector::new("TemporaryUsername");
    }

    /**
     * @param string $type
     * @param Request $request
     * @return FormError|FormRedirect|bool
     * @throws \RuntimeException
     * @throws \Exception
     */

    public function process(string $type, Request $request)
    {

        if (GOOGLE_RECAPTCHA_ENABLED)
            $this->addRecaptcha();

        if( $this->temporaryusername->exist( session_id() ) )
            $this->model->temporary = $this->getTemporaryUsername();

        if( $type == MVC_REQUEST_POST )
        {

            if( $this->check( $request->data ) == false )
                return new FormError(FORM_ERROR_MISSING,"Please fill out all the missing fields");
            else
            {

                $form = $this->pickKeys( $request->data, true);

                if( GOOGLE_RECAPTCHA_ENABLED )
                    if( $this->checkRecaptcha( $form ) == false )
                        return new FormError( FORM_ERROR_GENERAL, "Google response invalid");

                $result = $this->checkRegister( $form );

                if( is_array( $result ) )
                    return new FormError($result['type'],$result['value']);
                else
                {

                    $userid = $this->user->register( $this->getTemporaryUsername(), $form->email, $form->password );

                    if( $this->user->hasEmail( $form->email ) == false )
                        throw new \RuntimeException("Failed to register");

                    if( $this->temporaryusername->exist( session_id() ) )
                        $this->temporaryusername->delete( session_id() );

                    try
                    {

                        $this->autoexec->execute("register", [ "userid" => $userid, "message" => "Registered user: " . $userid ] );
                    }
                    catch ( \RuntimeException $error )
                    {

                        return new FormError("While your account was created, we were unable to run some back end work on your account so it" .
                        "could potentially have been set up incorrectly. Please get in contact with support if you get further issues, " .
                        "and give them this error: " . $error->getMessage());
                    }

                    return new FormRedirect( "register/success", 2 );
                }
            }
        }

        return true;
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     * @throws \RuntimeException
     */

    public function authentication(string $type, Request $request)
    {

        $application = Container::get('application');

        if( $application->session->isLoggedIn() )
            return false;

        return true;
    }

    /**
     * @param $form
     * @return array|bool
     */

    private function checkRegister( $form )
    {

        if( $this->user->hasEmail( $form->email ) )
            return([
                "type" => FORM_ERROR_INCORRECT,
                "value" => "Email has already been taken"
            ]);

        if( $this->checkPassword( $form->password, $form->confirm_password ) == false )
            return([
                "type" => FORM_ERROR_GENERAL,
                "value" => "Your password is too weak, please revise."
            ]);

       return true;
    }

    /**
     * @return \Illuminate\Support\Collection|string
     */

    private function getTemporaryUsername()
    {

        if( $this->temporaryusername->exist( session_id() ) == false )
            return $this->temporaryusername->generate();
        else
            return $this->temporaryusername->get( session_id() )->username;
    }

    /**
     * @param $password
     * @param $confirm_password
     * @return bool
     */

    private function checkPassword( $password, $confirm_password )
    {

        if( $password !== $confirm_password )
            return false;

        if( strlen( $password  ) < ACCOUNT_PASSWORD_MIN )
            return false;

        if( ACCOUNT_PASSWORD_STRICT )
        {

            foreach( $this->password_strict_stems as $stem )
            {

                if( !preg_match( $stem, $password ) )
                    return false;
            }

            return true;
        }
        else
            return true;
    }
}