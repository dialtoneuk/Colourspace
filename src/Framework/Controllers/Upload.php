<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/07/2018
 * Time: 20:41
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Container;
use Colourspace\Framework\Amazon;
use Colourspace\Framework\Session;
use Colourspace\Framework\Track;
use Colourspace\Framework\UploadStream;
use Colourspace\Framework\User;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Util\Controller\FormError;
use Colourspace\Framework\Util\Controller\FormRedirect;
use Colourspace\Framework\Util\Controller\Permissions;
use Colourspace\Framework\Util\Controller\UserPermissions;
use Colourspace\Framework\Util\Format;
use Colourspace\Framework\Util\Markdown;
use Colourspace\Framework\Util\MediaOperator;
use Colourspace\Framework\Wave;
use FFMpeg\Media\Waveform;
use flight\net\Request;

/**
 * Class Upload
 * @package Colourspace\Framework\Controllers
 * @deprecated To be replaced
 */

class Upload extends Normal
{

    /**
     * @var UploadStream
     */

    protected $uploads;

    /**
     * @var Amazon
     */

    protected $amazon;

    /**
     * @var Permissions;
     */

    protected $permissions;

    /**
     * @var UserPermissions
     */

    protected $userpermissions;

    /**
     * @var User
     */

    protected $user;

    /**
     * @var Track
     */

    protected $track;

    /**
     * @var Session
     */

    protected $session;

    /**
     * @var Markdown
     */

    protected $markdown;

    /**
     * @throws \RuntimeException
     */

    public function keyRequirements()
    {

        $array = [
            "name",
            "description",
            "privacy"
        ];

        if( GOOGLE_RECAPTCHA_ENABLED )
            $array[] = "g-recaptcha-response";

        return( $array );
    }

    /**
     * @throws \RuntimeException
     */

    public function before()
    {

        parent::before();

        $this->session = Container::get('application')->session;
        $this->uploads = Collector::new("UploadStream");

        if( UPLOADS_LOCAL == false )
            $this->amazon = Collector::new("Amazon");

        $this->user = Collector::new("User");
        $this->track = Collector::new("Track");
        $this->markdown = Collector::new("Markdown", "Colourspace\\Framework\\Util\\");
        $this->userpermissions = Collector::new("UserPermissions", "Colourspace\\Framework\\Util\\Controller\\");
        $this->permissions = Collector::new("Permissions", "Colourspace\\Framework\\Util\\Controller\\");
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool|FormError|FormRedirect
     * @throws \Delight\FileUpload\Throwable\InputNotSpecifiedError
     * @throws \RuntimeException
     * @throws \RuntimeException
     * @throws \Exception
     */

    public function process(string $type, Request $request)
    {

        if (GOOGLE_RECAPTCHA_ENABLED)
            $this->addRecaptcha();

        if( $type == MVC_REQUEST_POST )
        {

            if( $this->check( parent::process($type, $request), false ) == false )
                return new FormError( FORM_ERROR_MISSING, "Please fill out all the missing fields" );
            else
            {

                $form = $this->pickKeys( $request->data );

                if( empty( $form->name ) )
                    return new FormError( FORM_ERROR_MISSING, "Please fill out all the missing fields" );

                if( empty( $form->privacy ) )
                    return new FormError( FORM_ERROR_MISSING, "Please fill out all the missing fields" );

                if( GOOGLE_RECAPTCHA_ENABLED )
                    if ($this->checkRecaptcha($form) == false)
                        return new FormError( FORM_ERROR_GENERAL, "Google response invalid");

                $result = $this->verify( $form );

                if( is_array( $result ) )
                    return new FormError( $result['type'],$result['value'] );
                else
                {

                    try
                    {

                        $result = $this->upload( $form );
                    }
                    catch ( \RuntimeException $error )
                    {

                        return new FormError(FORM_ERROR_GENERAL, $error->getMessage() );
                    }

                    if( UPLOADS_LOCAL )
                        $streams = [
                            $result["type"] => UPLOADS_TEMPORARY_DIRECTORY . $result["key"]
                        ];
                    else
                        $streams = [
                            $result["type"] => AMAZON_BUCKET_URL . $result["key"]
                        ];

                    $trackid = $this->track->create(
                        $result['userid'],
                        $streams,
                        $form->name,
                        $this->track->getMetadataArray(
                            $result["username"],
                            $this->markdown->markup( $form->description ),
                            [],
                            null,
                            $result['type']
                        ));

                    if( $this->track->find( $form->name ) == false )
                        throw new \RuntimeException("Failed to add track");

                    $this->doPostUploadWaveform( $trackid, $result );

                    if( UPLOADS_LOCAL == false )
                        @unlink( COLOURSPACE_ROOT . $result["temp"] );

                    return new FormRedirect("/listen/" . $trackid );
                }
            }
        }

        return true;
    }
    /**
     * @param string $type
     * @param Request $request
     * @return bool
     */

    public function authentication(string $type, Request $request)
    {

        return parent::authentication($type, $request);
    }


    /**
     * @param $trackid
     * @param $result
     * @throws \RuntimeException
     */

    public function doPostUploadWaveform( $trackid, $result )
    {

        $media = new MediaOperator( $result["temp"] );
        $wave = $media->getWaveform();

        $this->saveWaveform( $wave, $result['filename'] . ".png", $trackid, UPLOADS_WAVEFORMS_LOCAL );

        if( UPLOADS_WAVEFORMS_LOCAL )
            $path = "files/waveforms/" . $result['filename'] . ".png";
        else
            $path = AMAZON_BUCKET_URL . $result['filename'] . ".png";

        $this->track->updateMetadata( $trackid, [
            "waveform" =>  $path
        ]);
    }

    /**
     * @param $form
     * @return array
     * @throws \Delight\FileUpload\Throwable\Error
     * @throws \Delight\FileUpload\Throwable\InputNotSpecifiedError
     * @throws \Delight\FileUpload\Throwable\TempDirectoryNotFoundError
     * @throws \Delight\FileUpload\Throwable\TempFileWriteError
     * @throws \Delight\FileUpload\Throwable\UploadCancelledException
     */

    private function upload( $form )
    {

        $result = null;

        try
        {

            $user = $this->user->get( $this->session->userid() );
            $limits = $this->getGroupLimits();
            $this->uploads->setHeader();

            if( $limits !== null )
            {

                if( $limits[ GROUPS_FLAG_MAXSIZE ] != -1 )
                    $this->uploads->setMaxFilesize( $limits[ GROUPS_FLAG_MAXSIZE ] );
                else
                    $this->uploads->setMaxFilesize( UPLOADS_MAX_SIZE_GLOBAL );

                if( $limits[ GROUPS_FLAG_LOSSLESS ] )
                    $allowed = [
                        "mp3", "wav", "flac"
                    ];
                else
                    $allowed = [
                        "mp3"
                    ];

                $this->uploads->setAllowedExtensions( $allowed );
            }
            else
                $this->uploads->setAllowedExtensions( ["mp3"] );

            $this->uploads->setFileName( $this->generate() );
            $result = $this->uploads->save();

            if( is_int( $result ) )
            {

                $errors = [
                    UPLOADS_ERROR_NOT_FOUND => "File not found.",
                    UPLOADS_ERROR_FILENAME  => "Filename is invalid.",
                    UPLOADS_ERROR_TOO_LARGE => "File size is over the permitted maximum allowance.",
                    UPLOADS_ERROR_EXTENSION => "Files extension is not permitted, please check that you can upload in lossless formats or check you are uploading music.",
                    UPLOADS_ERROR_CANCELLED => "Upload was cancelled",
                ];

                throw new \RuntimeException( $errors[ $result ] );
            }

            if( UPLOADS_LOCAL == false )
            {

                $this->put( $user->userid, $form->name, $result->getFilenameWithExtension(), $result->getPath(), null, [], $this->getContentType( $result->getExtension() ));

                if( $this->amazon->exists( AMAZON_S3_BUCKET, $result->getFilenameWithExtension() ) == false )
                    throw new \RuntimeException("Amazon failed to put object in bucket");
            }

            $return = [
                "userid"    => $user->userid,
                "username"  => $user->username,
                "temp"      => UPLOADS_TEMPORARY_DIRECTORY . $result->getFilenameWithExtension(),
                "type"      => $result->getExtension(),
                "filename"  => $result->getFilename(),
                "key"       => $result->getFilenameWithExtension()
            ];

            return( $return );
        }
        catch ( \RuntimeException $error )
        {

            if( $result == null || is_int( $result ) )
                throw $error;
            else
            {

                @unlink( $result->getPath() );
                throw $error;
            }
        }
    }

    /**
     * @param $userid
     * @param $trackname
     * @param $filename
     * @param $sourcefile
     * @param null $bucket
     * @param array $metadata
     * @param null $content_type
     */

    private function put( $userid, $trackname, $filename, $sourcefile, $bucket=null, $metadata=[], $content_type = null )
    {

        $metadata = array_merge( $metadata, [
            "userid"        => $userid,
            "trackname"     => $trackname,
            "uploadtime"    => Format::timestamp(),
            "ipaddress"     => $_SERVER["REMOTE_ADDR"]
        ]);

        if( $bucket == null )
            $bucket = AMAZON_S3_BUCKET;

        $this->amazon->put( $bucket, $filename, $sourcefile, $metadata, $content_type );
    }

    /**
     * @param $type
     * @return mixed
     */

    private function getContentType( $type )
    {

        $types = [
            "mp3"   => "audio/mp3",
            "wav"   => "audio/wav",
            "flac"  => "audio/flac"
        ];

        return( $types[ $type ] );
    }

    /**
     * @param Waveform $wave
     * @param $filename
     * @param $trackid
     * @param bool $local
     */

    private function saveWaveform( Waveform $wave, $filename, $trackid, $local=true )
    {

        if( file_exists( COLOURSPACE_ROOT . "files/waveforms/") == false )
            mkdir( COLOURSPACE_ROOT . "files/waveforms/");

        $path = "files/waveforms/" . $filename;

        if( $local == true )
            $wave->save( COLOURSPACE_ROOT . $path );
        else
        {

            $wave->save( COLOURSPACE_ROOT . $path );
            $this->amazon->put( AMAZON_S3_BUCKET, $filename, COLOURSPACE_ROOT . $path, ["trackid" => $trackid], "image/png" );

            unlink( COLOURSPACE_ROOT . $path  );
        }
    }

    /**
     * @param $path
     * @param bool $svg
     * @return mixed
     * @throws \RuntimeException
     */

    private function generateWaveform( $path, $svg=true )
    {

        $waveform = new Wave( $path );

        if( $svg )
            return( $waveform->svg() );
        else
            return( $waveform->generate() );
    }

    /**
     * @return string
     */

    private function generate()
    {

        return( uniqid(rand(), true) );
    }

    /**
     * @return array
     * @throws \RuntimeException
     */

    private function getGroupLimits()
    {

        $keys = [
            GROUPS_FLAG_LOSSLESS,
            GROUPS_FLAG_ADMIN,
            GROUPS_FLAG_MAXLENGTH,
            GROUPS_FLAG_MAXSIZE
        ];

        $flags = [];

        if( $this->userpermissions->hasUserPermissions() )
        {

            foreach ( $keys as $key=>$value )
                $flags[ $value ] = $this->userpermissions->getPermission( $value );
        }
        else
        {

            foreach ( $keys as $key=>$value )
                $flags[ $value ] = $this->permissions->getPermission( $value );
        }


        return( $flags );
    }

    /**
     * @param $form
     * @return array|bool
     */

    private function verify( $form )
    {

        if( $this->checkName( $form->name ) == false )
            return([
                "type" => FORM_ERROR_INCORRECT,
                "value" => "Your name cannot contain any special characters"
            ]);

        if( strlen( $form->name ) > TRACK_NAME_MAXLENGTH )
            return([
                "type" => FORM_ERROR_INCORRECT,
                "value" => "Your name is too long! Shorten it down below " . TRACK_NAME_MAXLENGTH . " characters."
            ]);

        if( $this->track->find( $form->name )->isNotEmpty() )
            return([
                "type" => FORM_ERROR_INCORRECT,
                "value" => "Name is already taken, track names need to be unique!"
            ]);

        if( $form->privacy != ( TRACK_PRIVACY_PUBLIC || TRACK_PRIVACY_PRIVATE || TRACK_PRIVACY_PERSONAL ) )
            return([
                "type" => FORM_ERROR_INCORRECT,
                "value" => "Unknown privacy type"
            ]);

        return true;
    }

    /**
     * @param $name
     * @return bool
     */

    private function checkName( $name )
    {

        if( preg_match("'/[\'^£$%&*()}{@#~?><>,|=_+¬-]/'", $name ) )
            return false;

        return true;
    }
}