<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 15:19
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Framework\Controller;
use Colourspace\Framework\Util\Debug;
use flight\net\Request;

class Normal extends Controller
{

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     * @throws \RuntimeException
     */

    public function process(string $type, Request $request)
    {

        if( DEBUG_ENABLED )
            Debug::message("Default contrller process called");

        return true;
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     */

    public function authentication(string $type, Request $request)
    {

       return true;
    }
}