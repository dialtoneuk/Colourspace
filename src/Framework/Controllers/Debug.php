<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 15:19
 */

namespace Colourspace\Framework\Controllers;


use Colourspace\Framework\Controller;
use Colourspace\Framework\Util\Controller\FormMessage;
use flight\net\Request;

class Debug extends Controller
{

    /**
     * @param string $type
     * @param Request $request
     * @return bool|FormMessage
     * @throws \RuntimeException
     */

    public function process(string $type, Request $request)
    {

        if( DEBUG_ENABLED == false )
            return false;

        return new FormMessage(FORM_MESSAGE_SUCCESS, "Successfully initiated controller");
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     */

    public function authentication(string $type, Request $request)
    {

       return true;
    }
}