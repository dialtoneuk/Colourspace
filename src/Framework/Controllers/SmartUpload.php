<?php

namespace Colourspace\Framework\Controllers;
use Colourspace\Framework\Controller;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 01:23
 */

use Colourspace\Framework\Uploads;
use Colourspace\Framework\Util\Controller\FormError;
use Colourspace\Framework\Util\Conventions\UploadData;
use flight\net\Request;
use Colourspace\Framework\Util\Debug;

class SmartUpload extends Controller
{

    /**
     * @var string
     */

    protected $class_name = "Normal";

    /**
     * @var UploadData
     */

    protected $object;

    /**
     * @var Uploads
     */

    protected $uploads;

    /**
     * Creates a new uploads class
     */

    public function before()
    {

        $this->uploads = new Uploads();

        if( $this->uploads->exist( $this->class_name ) == false )
            throw new \RuntimeException("class name does not exist in: " . __CLASS__ );

        parent::before();
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     */

    public function authentication(string $type, Request $request)
    {

        return parent::authentication($type, $request);
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool|FormError
     */

    public function process(string $type, Request $request)
    {

        //Allows higher up the chain to instead set the object
        if( isset( $this->object ) == false || empty( $this->object ))
            $object = Uploads::dataInstance([
                "settings" => [
                    "delete" => true
                ],
                "form" => $request->data->getData() ,
                "filename" => null
            ]);
        else
            $object = $this->object;

        if( $type == MVC_REQUEST_POST )
        {

            $result = $this->uploads->upload( $object, $this->userid(), $this->class_name, function()
            {

                if( Debug::isEnabled() )
                    Debug::message("Upload successful");
            });

            if( $result == false )
                return new FormError( $this->uploads->getLastError()->getMessage() );
        }

        return parent::process($type, $request);
    }
}