<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 16/07/2018
 * Time: 23:30
 */

namespace Colourspace\Framework\Types;


use Colourspace\Framework\Type;
use Colourspace\Framework\Util\Format;

class Twig extends Type
{

    /**
     * @var \Twig_Loader_Filesystem
     */

    protected $loader;

    /**
     * @var \Twig_Environment
     */

    protected $twig;

    /**
     * Twig constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->loader = new \Twig_Loader_Filesystem( TWIG_VIEW_FOLDER );
        $this->twig = new \Twig_Environment(  $this->loader, array(
            'cache' => 'views/cache/',
            'debug' => true
        ));
        $this->twig->addExtension(new \Twig_Extension_Debug() );;
    }

    /**
     * @return mixed|void
     * @throws \RuntimeException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */

    public function process()
    {

        $array = $this->get();

        if ( empty( $array ) == false && is_array( $array ) )
        {

            if (isset($array['template']) == false)
                throw new \RuntimeException('template');

            if( str_contains( $array["template"], ".twig") == false )
            {

                if( count( explode(".", $array["template"] ) ) !== 1 )
                    throw new \RuntimeException("Twig files can only be .twig files");

                $array["template"] = $array["template"] . ".twig";
            }

            if( isset( $array["page"] ) == false )
            {

                $array["page"] = [
                    "header" => [
                        Format::asset("js", WEBSITE_JQUERY )
                    ],
                    "footer" => [
                        Format::asset("js",WEBSITE_BOOTSTRAP4 )
                    ],
                    "title" => WEBSITE_TITLE
                ];
            }

            $object = array_merge($array['model'], $array["page"]);

            $template = $this->twig->load( $array["template"] );

            echo( $template->render( $object ) );
            exit( 0 );
        }
    }
}