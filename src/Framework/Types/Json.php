<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 08/07/2018
 * Time: 21:17
 */

namespace Colourspace\Framework\Types;

use Colourspace\Framework\Type;
use Flight;

class Json extends Type
{

    /**
     * @return mixed|void
     */

    public function process()
    {

        Flight::json( $this->array );
    }

    /**
     * @param bool $array
     * @return mixed
     */

    public function get($array = true)
    {
        return parent::get($array);
    }
}