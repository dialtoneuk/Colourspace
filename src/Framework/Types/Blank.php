<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 08/07/2018
 * Time: 21:17
 */

namespace Colourspace\Framework\Types;

use Colourspace\Framework\Util\Debug;
use Colourspace\Framework\Type;
use Flight;

class Blank extends Type
{

    /**
     * @return mixed|void
     * @throws \RuntimeException
     */

    public function process()
    {

        if( DEBUG_ENABLED )
            Debug::message( print_r( $this->array ) );

        Flight::notFound();
    }

    /**
     * @param bool $array
     * @return mixed
     */

    public function get($array = true)
    {
        return parent::get($array);
    }
}