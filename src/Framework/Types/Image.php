<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 08/07/2018
 * Time: 21:17
 */

namespace Colourspace\Framework\Types;

use Colourspace\Framework\Type;


class Image extends Type
{

    /**
     * @return mixed|void
     */

    public function process()
    {

        $array = $this->get();

        //TODO: Research how to output an image and hook up the colours
    }

    /**
     * @param bool $array
     * @return mixed
     */

    public function get($array = true)
    {
        return parent::get($array);
    }
}