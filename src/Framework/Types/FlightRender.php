<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 08/07/2018
 * Time: 21:17
 */

namespace Colourspace\Framework\Types;


use Colourspace\Container;
use Colourspace\Framework\Util\Format;
use Colourspace\Framework\Util\ScriptBuilder;
use Colourspace\Framework\Util\Debug;
use Colourspace\Framework\Type;
use Flight;


class FlightRender extends Type
{

    /**
     * @var array
     */

    private $allowed_payload_values = [
        "css",
        "title",
        "header",
        "footer",
        "meta"
    ];

    /**
     * @var ScriptBuilder
     */

    protected $script_builder;

    /**
     * @var array
     */

    protected $array = [];

    /**
     * Page constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->script_builder = new ScriptBuilder();
    }

    /**
     * @param array $array
     * @return mixed|void
     * @throws \RuntimeException
     */

    public final function setArray(array $array)
    {

        if (isset($array["page"]) == false)
            $array["page"] = [];

       $this->array = $array;
    }

    /**
     * @return mixed|void
     * @throws \RuntimeException
     */

    public function process()
    {

        $array = $this->get();

        if (empty($array) && is_array($array) == false)
            throw new \RuntimeException("Invalid array");

        if (isset($array['template']) == false)
            throw new \RuntimeException('No template');

        if (isset($array['model']) == false)
            throw new \RuntimeException('No model');

        if( Container::get("application")->session->isActive() )
            if( isset( $_SESSION["response"] ) )
            {

                $array["model"]["response"] = $_SESSION["response"];
                unset( $_SESSION["response"] );
            }


        if( isset($array["page"]) == false )
            $array["page"] = [];

        if( isset( $array["page"]['title'] ) )
            $title = $array["page"]['title'];
        else
            $title = WEBSITE_TITLE;

        $array["page"] = array_merge($array["page"], [
            "header" => [
                Format::asset("js", WEBSITE_JQUERY)
            ],
            "title" => $title
        ]);

        if (SCRIPT_BUILDER_ENABLED)
        {
            $this->buildScripts();

            if (isset($array["page"]["footer"]))
                $array["page"]["footer"][] = Format::asset("js", SCRIPT_BUILDER_COMPILED);
            else
                $array["page"]["footer"] = [
                    Format::asset("js", SCRIPT_BUILDER_COMPILED)
                ];
        }

        if( $this->checkPage( $array["page"] ) == false )
            throw new \RuntimeException("Unsafe page payload: " . $array["template"] );

        $content = [
            "model" => $array["model"],
            "page" => $array["page"],
            "template" => $array["template"]
        ];

        $this->flightConfig( $content );

        Flight::render($content['template']);
    }

    /**
     * @param $content
     * @throws \RuntimeException
     */

    private function flightConfig( $content )
    {

        if (FLIGHT_CONTENT_OBJECT)
        {

            $object = json_decode(json_encode($content));

            if (empty($object))
                throw new \RuntimeException("Failed to decode json: " . json_last_error_msg());

            Flight::view()->set(FLIGHT_MODEL_DEFINITION, $object->model);
            Flight::view()->set(FLIGHT_PAGE_DEFINITION, $object->page);
        }
        else
        {

            Flight::view()->set(FLIGHT_MODEL_DEFINITION, $content["model"]);
            Flight::view()->set(FLIGHT_PAGE_DEFINITION, $content["page"]);
        }

        if(FLIGHT_SET_GLOBALS)
        {

            Flight::view()->set("url_root", COLOURSPACE_URL_ROOT);
            Flight::view()->set("document_root", COLOURSPACE_ROOT);

            if (DEBUG_ENABLED)
                Flight::view()->set("debug_messages", Debug::getMessages());
        }
    }

    /**
     * @param $payload
     * @return bool
     */

    private function checkPage( $payload )
    {

        $array = array_flip( $this->allowed_payload_values );

        foreach( $payload as $key=>$value )
        {

            if( isset( $array[ $key ] ) == false )
            {

                return false;
            }

        }

        return true;
    }

    /**
     * @throws \RuntimeException
     */

    private function buildScripts()
    {

        if( SCRIPT_BUILDER_ENABLED == false )
            return;

        $this->script_builder->build();
    }
}