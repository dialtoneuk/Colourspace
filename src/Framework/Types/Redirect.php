<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 08/07/2018
 * Time: 21:17
 */

namespace Colourspace\Framework\Types;

use Colourspace\Container;
use Colourspace\Framework\Type;
use Flight;

class Redirect extends Type
{
    /**
     * @return mixed|void
     */

    public function process()
    {

        if( isset( $this->array["model"]["response"] ) )
            if(Container::get("application")->session->isActive() )
                $_SESSION["response"] = $this->array["model"][ "response" ];

        if( isset( $this->array["page"]["url"] ) == false )
            $this->array["page"]["url"] = COLOURSPACE_URL_ROOT;

        Flight::redirect( $this->array["page"]["url"] );
    }

    /**
     * @param bool $array
     * @return mixed
     */

    public function get($array = true)
    {
        return parent::get($array);
    }
}