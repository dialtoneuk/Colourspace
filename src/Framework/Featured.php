<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 20/07/2018
 * Time: 18:22
 */

namespace Colourspace\Framework;


use Colourspace\Framework\Util\DirectoryOperator;

class Featured
{

    /**
     * @var array
     */

    public $features = [];

    /**
     * Featured constructor.
     * @param bool $auto_init
     * @throws \RuntimeException
     */

    public function __construct( $auto_init = true )
    {

        if( $auto_init )
            $this->initialize();
    }

    /**
     * @param $name
     * @return mixed
     * @throws \RuntimeException
     */

    public function get( $name )
    {

        if( $this->hasInit() == false )
            $this->initialize();

        return( $this->features[ $name ] );
    }

    /**
     * @param $name
     * @return bool
     */

    public function has( $name )
    {

        return( isset( $this->features[ $name ] ) );
    }

    /**
     * @param $name
     * @param $values
     * @param bool $write
     * @throws \RuntimeException
     */

    public function set( $name, $values, $write=true )
    {

        $this->features[ $name ] = array_merge( $this->features[ $name ], [ "name" => $name, "values" => $values ] );

        if( $write )
            $this->write( $name );

        $this->refresh( $name );
    }

    /**
     * @param $name
     * @param $value
     * @throws \RuntimeException
     */

    public function add( $name, $value )
    {

        $this->set( $name, [ $value ] );
    }

    /**
     * @param $name
     * @param bool $write
     * @throws \RuntimeException
     */

    public function refresh( $name, $write=true )
    {

        $this->features[ $name ]["updated"] = time();

        if( $write )
            $this->write( $name );
    }
    /**
     * @param $name
     * @param $values
     * @param array $options
     * @throws \RuntimeException
     */

    public function create( $name, $values, array $options )
    {

        if( isset( $this->features[ $name ] ) )
            throw new \RuntimeException("Features already exists");

        if( isset( $options["name"] ) )
            unset( $options["name"] );

        if( isset( $options["values"] ) )
            unset( $options["values"] );

        if( isset( $options["updated"] ) )
            unset( $options["updated"] );

        $this->features[ $name ] = array_merge( [
            "name" => $name,
            "updated" => time(),
            "values" => $values
        ], $options );

        $this->write( $name );
    }

    /**
     * @throws \RuntimeException
     */

    public function initialize()
    {

        $files = $this->scrape();

        if( empty( $files ) )
            throw new \RuntimeException("Files are empty");

        foreach( $files as $file )
        {

            $array = json_decode( file_get_contents( COLOURSPACE_ROOT . FEATURED_ROOT . $file ), true );

            if( isset( $array["name"] ) == false || isset( $array["values"] ) == false )
                throw new \RuntimeException("Invalid array format");

            if( $array['name'] == "ignore" )
                continue;

            if( isset( $this->features[ $array['name']] ) )
                throw new \RuntimeException("Name already set, needs to be unique: " . $file );

            $this->features[ $array['name'] ] = $array;
        }

        if( json_last_error() !== JSON_ERROR_NONE )
            throw new \RuntimeException( json_last_error_msg() );
    }
    /**
     * @return bool
     */

    private function hasInit()
    {

        if( empty( $this->features ) )
            return false;

        return true;
    }

    /**
     * @return array
     * @throws \RuntimeException
     */

    private function scrape()
    {

        $dir = new DirectoryOperator( FEATURED_ROOT );

        if( $dir->isEmpty() )
            throw new \RuntimeException("No features found");

        return( $dir->omit( $dir->search([".json"] ) ) );
    }

    /**
     * @param $name
     * @throws \RuntimeException
     */

    private function write( $name )
    {

        if( count( explode(".", $name ) ) != 1 )
            throw new \RuntimeException("Invalid name, must not be file");

        if( isset( $this->features[ $name ] ) )
            throw new \RuntimeException("Invalid feature: " . $name );

        file_put_contents( COLOURSPACE_ROOT . FEATURED_ROOT . $name . ".json", $this->features[ $name ] );
    }
}