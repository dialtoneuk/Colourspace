<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 18/07/2018
 * Time: 20:43
 */

namespace Colourspace\Framework;

use Colourspace\Database\Tables\Balances;
use Colourspace\Framework\Util\Format;

class Balance
{

    /**
     * @var Balances
     */

    protected $table;

    /**
     * Balance constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->table = new Balances();
    }

    /**
     * @param $balanceid
     * @return \Illuminate\Support\Collection
     */

    public function get( $balanceid )
    {

        return( $this->table->get( $balanceid )->first() );
    }

    /**
     * @param $balanceid
     * @return bool
     */

    public function exists( $balanceid )
    {

        return( $this->table->exist( $balanceid ) );
    }

    /**
     * @param $userid
     * @return bool
     */

    public function hasBalance( $userid )
    {

        if( $this->table->search("userid", $userid )->isEmpty() )
            return false;

        return true;
    }

    /**
     * Returns the first balance
     *
     * @param $userid
     * @return mixed
     */

    public function getBalance( $userid )
    {

        return( $this->getAllBalances( $userid )->first() );
    }

    /**
     * @param $userid
     * @return \Illuminate\Support\Collection
     */

    public function getAllBalances( $userid )
    {

        return( $this->table->search("userid", $userid ));
    }

    /**
     * @param $balanceid
     * @param $amount
     * @return bool
     */

    public function afford( $balanceid, $amount )
    {

        if( ( $this->modify( $balanceid ) - $amount ) < 0 )
            return false;

        return true;
    }

    /**
     * @param $userid
     * @return mixed
     */

    public function getBalanceID( $userid )
    {

        return( $this->getBalance( $userid )->balanceid );
    }

    /**
     * Gets or sets the ammount
     *
     * @param $balanceid
     * @param null $amount
     * @return int
     */

    public function modify($balanceid, $amount=null )
    {

        $balance = $this->get( $balanceid );

        if( $amount == null )
            $amount = 0;

        $this->table->update( [ $this->table->primary => $balanceid ], [ "amount" => $balance->amount + $amount ] );

        return( $balance->amount + $amount  );
    }

    /**
     * @param $userid
     * @param int $amount
     * @return int
     * @throws \RuntimeException
     */

    public function create( $userid, $amount=0 )
    {

        return($this->table->insert([
            COLUMN_USERID   => $userid,
            "amount"        => $amount,
            "creation"      => Format::timestamp()
        ], false ));
    }
}