<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 06/08/2018
 * Time: 16:41
 */

namespace Colourspace\Framework\Profiles;


use Colourspace\Framework\Profile;
use Colourspace\Framework\Balance as BalanceManager;


class Balance extends Profile
{

    /**
     * @var BalanceManager
     */

    protected $balance;

    /**
     * @var \Colourspace\Framework\Session
     */

    protected $session;

    /**
     * Balance constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        parent::__construct([
            [
                "Colourspace\\Framework\\", "Balance"
            ]
        ]);

        $this->balance = $this->class("Balance");
    }

    /**
     * @return null|void
     * @throws \RuntimeException
     */

    public function create()
    {

        if( $this->session->isLoggedIn() == false )
        {



            $this->objects = null;
        }
        else
        {

            $userid = $this->session->userid();

            if( $this->balance->hasBalance( $userid ) == false )
                $this->objects = null;
            else
            {

                $this->objects = $this->balance->getBalance( $userid );
            }
        }

        parent::create();
    }
}