<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 15/07/2018
 * Time: 16:37
 */

namespace Colourspace\Framework\Profiles;


use Colourspace\Framework\Profile;
use Colourspace\Framework\User;
use Colourspace\Framework\Verifications as VerifyClass;
use Colourspace\Container;

class Verifications extends Profile
{

    /**
     * @var User
     */

    protected $user;

    /**
     * @var VerifyClass
     */

    protected $verifications;

    /**
     * Verifications constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        parent::__construct(
            [
                [
                    "Colourspace\\Framework\\", "User"
                ],
                [
                    "Colourspace\\Framework\\", "Verifications"
                ]
            ]
        );

        $this->user = $this->class('User');
        $this->verifications = $this->class('Verifications');
}

    /**
     * @return null|void
     * @throws \RuntimeException
     */

    public function create()
    {

        if( $this->isLoggedIn() == false )
            $this->objects = null;
        else
        {

            $userid = Container::get('application')->session->userid();

            if( $this->verifications->userHas( $userid ) == false )
                $this->objects = null;
            else
            {

                $verifications = $this->verifications->user( $userid );

                foreach( $verifications as $verification )
                    $this->objects[ $verification->type ] = $verification;
            }
        }

        parent::create();
    }
}