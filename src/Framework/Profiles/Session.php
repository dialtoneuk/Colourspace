<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 04/07/2018
 * Time: 01:24
 */

namespace Colourspace\Framework\Profiles;


use Colourspace\Framework\Profile;

class Session extends Profile
{

    /**
     * @var \Colourspace\Framework\Session
     */

    protected $session;

    /**
     * Session constructor.
     * @param array $classes
     * @throws \RuntimeException
     */

    public function __construct(array $classes = [])
    {

        parent::__construct($classes);
    }

    /**
     * @return null|void
     * @throws \RuntimeException
     */

    public function create()
    {

        $this->objects = [
            "loggedin"  => $this->session->isLoggedIn(),
            "active"    => $this->session->isActive(),
            "sessionid" => session_id(),
            "objects"   => $_SESSION
        ];

        if( $this->isLoggedIn() )
            $this->objects['userid'] = $this->session->userid();

        parent::create();
    }
}