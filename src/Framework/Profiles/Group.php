<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 01/07/2018
 * Time: 00:50
 */

namespace Colourspace\Framework\Profiles;


use Colourspace\Framework\Profile;

use Colourspace\Framework\User as UserClass;
use Colourspace\Framework\Group as GroupClass;

class Group extends Profile
{

    /**
     * @var UserClass
     */

    protected $user;
    /**
     * @var GroupClass
     */
    protected $group;

    /**
     * User constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {


        parent::__construct(
            [
                [
                    "Colourspace\\Framework\\", "User"
                ],
                [
                    "Colourspace\\Framework\\", "Group"
                ]
            ]

        );

        $this->user = $this->class('User');
        $this->group = $this->class('Group');
    }

    /**
     * @return null|void
     * @throws \RuntimeException
     */

    public function create()
    {
        if( $this->isLoggedIn() == false )
            $this->objects = null;
        else
        {

            $user = $this->user->get( $this->getUserID() );

            if( $this->group->exist($user->group ) == false )
                $this->objects = null;
            else
            {

                $group = $this->group->get( $user->group );

                $this->objects = $group;
            }
        }

        parent::create();
    }
}