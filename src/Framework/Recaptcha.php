<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 03/07/2018
 * Time: 17:11
 */

namespace Colourspace\Framework;

use Phelium\Component\reCAPTCHA as Google;

/**
 * Class Recaptcha
 * @package Colourspace\Framework
 * @deprecated
 */

class Recaptcha
{

    /**
     * @var Google
     */

    protected $recaptcha;

    /**
     * Recaptcha constructor.
     * @param null $key
     * @param null $secret
     * @throws \RuntimeException
     */

    public function __construct( $key=null, $secret=null )
    {

        if( GOOGLE_RECAPTCHA_ENABLED )
        {

            $credentials = $this->getCredentials();

            if( $key == null )
                $key = $credentials->key;

            if( $secret == null )
                $secret = $credentials->secret;

            $this->recaptcha = new Google();
            $this->recaptcha->setSiteKey( $key );
            $this->recaptcha->setSecretKey( $secret );
        }

    }

    /**
     * @param $response
     * @return bool
     * @throws \Exception
     */

    public function isValid( $response )
    {

        if( GOOGLE_RECAPTCHA_ENABLED == false )
            return true;

        if( $this->recaptcha->isValid( $response ) )
            return true;

        return false;
    }

    /**
     * @return string
     */

    public function html()
    {

        if( GOOGLE_RECAPTCHA_ENABLED == false )
            return null;

        return( $this->recaptcha->getHtml() );
    }

    /**
     * @return string
     */

    public function script()
    {

        if( GOOGLE_RECAPTCHA_ENABLED == false )
            return null;

        return( $this->recaptcha->getScript() );
    }

    /**
     * @return mixed
     * @throws \RuntimeException
     */

    private function getCredentials()
    {

        if( file_exists( COLOURSPACE_ROOT . GOOGLE_RECAPTCHA_CREDENTIALS ) == false )
            throw new \RuntimeException("Credentials file does not exist");

        return( json_decode( file_get_contents( COLOURSPACE_ROOT . GOOGLE_RECAPTCHA_CREDENTIALS ) ) );
    }
}