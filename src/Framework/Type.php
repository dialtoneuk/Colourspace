<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 08/07/2018
 * Time: 21:17
 */

namespace Colourspace\Framework;


use Colourspace\Framework\Interfaces\ReturnsInterface;


use Flight;


abstract class Type implements ReturnsInterface
{

    /**
     * @var array
     */

    protected $array = [];

    /**
     * @param array $array
     * @return mixed|void
     * @throws \RuntimeException
     */

    public function setArray(array $array)
    {

        if (is_array($array) == false)
            throw new \RuntimeException("Must be array");

        $this->array = $array;
    }

    /**
     * @return mixed|void
     * @throws \RuntimeException
     */

    public function process()
    {

        Flight::json([ true ]);
    }

    /**
     * @param array $array
     * @throws \RuntimeException
     */

    public function add(array $array)
    {

        $this->array = array_merge( $this->array, $array );
    }

    /**
     * @param bool $array
     * @return mixed
     */

    public function get( $array=true )
    {

        return (json_decode(json_encode($this->array), $array));
    }
}