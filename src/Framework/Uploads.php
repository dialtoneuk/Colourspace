<?php

namespace Colourspace\Framework;



/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 29/08/2018
 * Time: 21:06
 */

use Colourspace\Framework\Interfaces\Upload;
use Colourspace\Framework\Util\Collection;
use Colourspace\Framework\Util\Conventions\UploadData;

/**
 * Class Uploads
 * @package Colourspace\Framework
 */

class Uploads extends Collection
{

    /**
     * Uploads constructor.
     * @param string $filepath
     * @param string $namespace
     * @param bool $auto_create
     */

    public function __construct($filepath=UPLOADS_FILEPATH, $namespace=UPLOADS_NAMESPACE, bool $auto_create = true)
    {

        parent::__construct($filepath, $namespace, $auto_create);
    }

    /**
     * @param $class_name
     * @param $userid
     * @param UploadData $data
     * @param callable|null $callback
     * @return bool
     */

    public function upload( UploadData $data, $userid, $class_name, callable $callback=null )
    {

        if( $this->getLastError() !== null )
            $this->setLastError();

        if ( is_array( $data ) == false && is_object( $data ) == false )
            throw new \RuntimeException("invalid type for data");

        $instance = $this->constructor->get( $class_name );

        /** @var $instance Upload */

        if( $instance instanceof Upload == false )
            throw new \RuntimeException("invalid instance type");

        try
        {

            $instance->before( $userid, $data );

            if( $instance->authenticate( $data, $userid ) == false )
                return false;

            $result = $instance->upload( $data, $userid );

            $instance->after( $result, $data, $userid );

        }
        catch ( \RuntimeException $exception )
        {

            $this->setLastError( $exception );
            return false;
        }

        if( $callback !== null )
            $callback();

        return true;
    }

    /**
     * @param array $values
     * @return UploadData
     */

    public static function dataInstance( array $values )
    {

        return( new UploadData( $values ) );
    }
}