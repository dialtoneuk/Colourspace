<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 16:49
 */

namespace Colourspace\Framework;


use Colourspace\Container;
use Colourspace\Framework\Interfaces\Model as ModelInterface;
use Colourspace\Framework\Interfaces\ProfileInterface;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Util\Controller\FormError;
use Colourspace\Framework\Util\Controller\FormRedirect;
use Colourspace\Framework\Util\Controller\FormMessage;
use Colourspace\Framework\Util\Debug;

/**
 * @property array|object page
 * @property array|object temporary
 * @property array|object items
 * @property array|object profiles
 */
abstract class Model implements ModelInterface
{

    /**
     * @var \stdClass
     */

    public $object;

    /**
     * Model constructor.
     */

    public function __construct()
    {
        $this->object = new \stdClass();
    }

    /**
     * @param array $profiles
     * @throws \RuntimeException
     */

    public function setProfiles( array $profiles )
    {

        if( isset( $this->object->profiles ) == false )
            $this->object->profiles = [];

        foreach( $profiles as $class=>$value )
        {

            if( isset( $value['loggedin'] ) && $value['loggedin'] )
                if( Container::get('application')->session->isLoggedIn() == false )
                    continue;

            $this->object->profiles[ $class ] = Collector::new( $class,"Colourspace\\Framework\\Profiles\\");
        }
    }

    /**
     * @throws \RuntimeException
     */
    public function processProfiles()
    {

        if( DEBUG_ENABLED )
            Debug::message("Processing Profiles" );

        if( isset( $this->object->profiles ) == false || empty( $this->object->profiles ) )
            return;

        foreach( $this->object->profiles as $class=>$instance )
        {

            /** @var ProfileInterface $instance */

            if( $instance instanceof ProfileInterface == false )
                throw new \RuntimeException("Profile is invalid");

            $instance->create();

            //Stops double classes
            unset( $this->object->profiles[ $class ] );
            $this->object->profiles[ strtolower( $class ) ] = $instance->toArray();

            if( DEBUG_ENABLED )
                Debug::message("Profile Processed: " . $class );
        }
    }

    /**
     * @throws \RuntimeException
     */

    public function startup()
    {

        $this->processProfiles();

        if( DEBUG_ENABLED )
            Debug::message("Startup called in model: " .  get_called_class() );
    }

    /**
     * @param $name
     * @return mixed
     */

    public function __get($name)
    {

        return( $this->object->$name );
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     */

    public function __set($name, $value)
    {

        return( $this->object->$name = $value );
    }

    /**
     * @param $name
     * @return bool
     */

    public function __isset($name)
    {

        return( isset( $this->object->$name ) );
    }

    /**
     * @return mixed
     */

    public function toArray()
    {

        return ( json_decode( json_encode( $this->object ), true ) );
    }

    /**
     * @return string
     */

    public function toJson()
    {

        return( json_encode( $this->object ) );
    }

    /**
     * @param $type
     * @param $value
     * @throws \RuntimeException
     */

    public function formError($type, $value)
    {

        $message = new FormError( $type, $value );

        $this->response( $message->get() );
    }

    /**
     * @param $type
     * @param $value
     * @throws \RuntimeException
     */

    public function formMessage($type, $value)
    {

       $message = new FormMessage( $type, $value );

       $this->response( $message->get() );
    }

    /**
     * @param array $response
     */

    public function response( array $response )
    {

        $this->object->response = $response;
    }

    /**
     * @param $type
     * @param $value
     * @throws \RuntimeException
     */

    public function formRedirect($type, $value)
    {

        $message = new FormRedirect( $type, $value );

        $this->response( $message->get() );
    }

    /**
     * @param $url
     * @param $delay
     * @throws \RuntimeException
     */

    public function redirect($url, $delay)
    {

        $this->formRedirect( $url, $delay );
    }
}