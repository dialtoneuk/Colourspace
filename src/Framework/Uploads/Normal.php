<?php
namespace Colourspace\Framework\Uploads;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 29/08/2018
 * Time: 21:43
 */

use Colourspace\Framework\Upload;
use Colourspace\Framework\Util\Conventions\UploadData;
use Delight\FileUpload\File;

class Normal extends Upload
{

    /**
     * @param UploadData $data
     * @param $userid
     * @return bool|\Delight\FileUpload\File
     * @throws \Exception
     */

    public function upload(UploadData $data, $userid)
    {

        return parent::upload($data, $userid);
    }

    /**
     * @param $data
     * @param $userid
     * @return mixed|void
     */

    public function before( UploadData $data, $userid)
    {

        //Sets the extensions, will merge with the previous value.
        if( $data->query("settings","extensions") )
            $this->setExtensions( $data->arrayValue("settings","extensions") );

        //Sets the max filesizes. Will overwrite previous value.
        if( $data->query("settings","filesize") )
            $this->setMaxFileSize( $data->arrayValue("settings","filesize") );

        parent::before($data, $userid);
    }

    /**
     * @param File $file
     * @param UploadData $data
     * @param $userid
     */

    public function after(File $file, UploadData $data, $userid)
    {

        if( $data->isTrueAndExists("settings","delete" ) )
            @unlink( $file->getPath() );

        parent::after($file, $data, $userid);
    }

    /**
     * @param UploadData $data
     * @param $userid
     * @return bool
     */

    public function authenticate( UploadData $data, $userid): bool
    {

        if( $this->isLoggedIn() == false )
            return( false );

        //Returns true
        return parent::authenticate($data, $userid);
    }
}