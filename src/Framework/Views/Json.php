<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 16:53
 */

namespace Colourspace\Framework\Views;

use Colourspace\Framework\View;

class Json extends View
{


    /**
     * @param string $type
     * @return mixed|void
     * @throws \RuntimeException
     */

    public function before( $type="Json")
    {

        parent::before($type);
    }
}