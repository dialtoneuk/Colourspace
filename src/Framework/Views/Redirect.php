<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 16:53
 */

namespace Colourspace\Framework\Views;

use Colourspace\Framework\View;

class Redirect extends View
{


    /**
     * @param string $type
     * @return mixed|void
     * @throws \RuntimeException
     */

    public function before( $type="Redirect")
    {

        parent::before($type);
    }

    /**
     * @param null $template
     * @param null $payload
     * @return \Colourspace\Framework\Interfaces\ReturnsInterface|mixed
     */

    public function get($template = null, $payload = null)
    {

        if( $payload !== null )
        {

            if( isset( $payload["url"] ) == false )
                $url = $payload["url"];
            else
                $url = COLOURSPACE_URL_ROOT;
        }
        else
        {
            $url = COLOURSPACE_URL_ROOT;
        }

        //Adds the redirect url
        $this->type->add([ "url" => $url ]);

        return parent::get($template, $payload);
    }
}