<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 16:53
 */

namespace Colourspace\Framework\Views;

use Colourspace\Framework\View;

class Twig extends View
{


    /**
     * @param string $type
     * @return mixed|void
     * @throws \RuntimeException
     */

    public function before( $type="Twig")
    {

        parent::before($type);
    }
}