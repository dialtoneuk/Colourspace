<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 22:41
 */

namespace Colourspace\Framework;


use Colourspace\Container;
use Colourspace\Framework\Interfaces\ProfileInterface;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Util\Debug;

abstract class Profile implements ProfileInterface
{

    /**
     * This prevents us from creating multiple instances of the same class per executing and instead use ones stored in an array
     *
     * @var array
     */
    protected static $shared_classes = [];
    /**
     * @var \stdClass
     */
    protected $objects;

    /**
     * @var Session
     */

    protected $session;

    /**
     * Profile constructor.
     * @param array $classes
     * @throws \RuntimeException
     */

    public function __construct( $classes=[] )
    {

        if( Container::exist('application') == false )
                throw new \RuntimeException('Container has not been initiated');

            $this->objects = new \stdClass();

        if( empty( $classes ) == false )
            $this->processClasses( $classes );

        $this->session = Container::get('application')->session;
    }


    /**
     * @return mixed
     */


    public function toArray()
    {

        return( json_decode( json_encode( $this->objects ), true ));
    }

    /**
     * @return \stdClass
     */

    public function get()
    {

        return( $this->objects );
    }

    /**
     * @return null|void
     * @throws \RuntimeException
     */

    public function create()
    {

        if ( DEBUG_ENABLED )
            Debug::message("Profile create: " .  get_called_class());
    }

    /**
     * @param $name
     * @return mixed
     */

    public function __get($name)
    {

        return ( $this->objects->$name );
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     */

    public function __set($name, $value)
    {

        return ( $this->objects->$name = $value );
    }

    /**
     * @return bool
     * @throws \RuntimeException
     */

    public function isLoggedIn()
    {

        if( $this->session->isActive() == false )
            return false;

        if( $this->session->isLoggedIn() == false )
            return false;

        return true;
    }

    /**
     * @return int|null
     * @throws \RuntimeException
     */

    public function getUserID()
    {

        if( $this->isLoggedIn() == false )
            return null;

        return( $this->session->userid() );
    }

    /**
     * @param $class
     * @return mixed
     */

    public function class($class )
    {

        return( self::$shared_classes[ $class ] );
    }

    /**
     * @param $class
     * @return bool
     */

    public function hasClass( $class )
    {

        return( isset( self::$shared_classes[ $class ] ) );
    }

    /**
     * @param $classes
     * @param bool $collector
     * @throws \RuntimeException
     */

    private function processClasses( $classes, $collector=true )
    {

        foreach ( $classes as $class )
        {

            $namespace = $class[0];
            $class = $class[1];

            foreach( self::$shared_classes as $key=>$value )
            {

                if( $key == $namespace && $class == $value )
                    return;
            }

            if( $collector )
            {

                self::$shared_classes[ $class ] = Collector::new( $class, $namespace );
            }
            else
            {

                $real_namespace = $namespace . $class;

                if( class_exists( $real_namespace ) == false )
                    throw new \RuntimeException('Invalid class given');

                self::$shared_classes[ $class ] = new $real_namespace;
            }
        }
    }
}