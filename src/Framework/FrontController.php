<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 00:24
 */
namespace Colourspace\Framework;

use Colourspace\Framework\Interfaces\Controller;
use Colourspace\Framework\Interfaces\Model;
use Colourspace\Framework\Interfaces\ReturnsInterface;
use Colourspace\Framework\Interfaces\ViewInterface;
use Colourspace\Framework\Util\Constructor;
use Colourspace\Framework\Util\Debug;
use Colourspace\Framework\Util\Interfaces\Response;
use flight\net\Request;
use flight\net\Route;

class FrontController
{

    /**
     * @var Constructor
     */
    protected $models;
    /**
     * @var Constructor
     */
    protected $controllers;
    /**
     * @var Constructor
     */
    protected $views;

    /**
     * @var null
     */

    public $namespace;

    /**
     * @var null
     */

    public $filepath;

    /**
     * FrontController constructor.
     * @param bool $auto_initialize
     * @param null $namespace
     * @param null $filepath
     * @throws \RuntimeException
     */

    public function __construct( $auto_initialize = true, $namespace = null, $filepath = null )
    {

        if ( $namespace == null )
            $this->namespace = MVC_NAMESPACE;
        else
            $this->namespace = $namespace;

        if ( $filepath == null )
            $this->filepath = MVC_ROOT;
        else
            $this->filepath = $filepath;

        if( $auto_initialize == true )
            $this->initialize();
    }

    /**
     * @param Request $request
     * @param $payload
     * @return bool|ReturnsInterface|mixed
     * @throws \RuntimeException
     */

    public function process( Request $request, $payload )
    {

        if( isset( $payload["template"] ) == false || isset( $payload[ "mvc" ] ) == false )
            throw new \RuntimeException("Invalid payload");

        $template = $payload["template"];

        if( isset( $payload["payload"] ) )
            $custom_payload = $payload["payload"];
        else
            $custom_payload = null;

        $payload = $payload["mvc"];

        if( count( $payload ) !== 3 )
            throw new \RuntimeException('To many keys in payload');

        foreach ( $payload as $key=>$item )
        {

            if( $key != ( MVC_TYPE_MODEL or MVC_TYPE_CONTROLLER or MVC_TYPE_VIEW ) )
                throw new \RuntimeException('Unknown key type: ' . $key );

            if( $this->exist( $key, $item ) == false )
                throw new \RuntimeException('Class not found: ' . $key );
        }

        if( isset( $request->method ) == false )
            throw new \RuntimeException('Request method invalid');

        if( $request->method != ( MVC_REQUEST_POST or MVC_REQUEST_DELETE or MVC_REQUEST_GET or MVC_REQUEST_PUT) )
            throw new \RuntimeException('Request method invalid');

        /** @var Model $model */
        $model = $this->get(  MVC_TYPE_MODEL, $payload[ MVC_TYPE_MODEL ] );
        $controller = $this->get(  MVC_TYPE_CONTROLLER, $payload[ MVC_TYPE_CONTROLLER ] );
        $view = $this->get(  MVC_TYPE_VIEW, $payload[ MVC_TYPE_VIEW ] );

        $model->startup();
        $controller->setModel( $model );

        if( $controller->authentication( $request->method, $request ) == false )
            return false;
        else
        {

            $controller->before();

            /** @var Response $response */
            $response = $controller->process( $request->method, $request );

            if( $response instanceof Response )
                $model->response( $response->get() );
            else
            {

                if( is_bool( $response ) )
                {

                    if( $response )
                        $model->response( ["success" => true ] );
                    else
                        $model->response( ["success" => false ] );
                }
                else
                    throw new \RuntimeException("Unknown response type from controller. Either has to be response interface or bool");
            }

            $view->before();
            $view->setModel( $model );

            return $view->get( $template, $custom_payload );
        }
    }

    /**
     * @param Route $route
     * @param bool $array
     * @return Request|array
     */

    public function buildRequest( Route $route, $array=false )
    {

        $request = new Request();
        $request->init(["params" => $route->params] );

        if( empty( $request ) )
            throw new \RuntimeException('Flight has not been started');

        if( strtolower( $request->method ) != ( MVC_REQUEST_POST or  MVC_REQUEST_DELETE or MVC_REQUEST_GET or MVC_REQUEST_PUT) )
            throw new \RuntimeException('Invalid method');

        if( $array )
            return( json_decode( json_encode( $request ) ) );

        return $request;
    }


    /**
     * @param string $type
     * @param string $class_name
     * @return bool
     */

    public function exist( string $type, string $class_name )
    {

        $class_name = strtolower( $class_name );

        switch ( $type )
        {

            case MVC_TYPE_MODEL:
                if( $this->models->exist( $class_name ) == false )
                    return false;
                break;
            case MVC_TYPE_VIEW:
                if( $this->views->exist( $class_name ) == false  )
                    return false;
                break;
            case MVC_TYPE_CONTROLLER:
                if( $this->controllers->exist( $class_name ) == false  )
                    return false;
                break;
        }

        return true;
    }

    /**
     * @param string $type
     * @param string $class_name
     * @return Model|Controller|ViewInterface
     */

    public function get( string $type, string $class_name )
    {

        $class_name = strtolower( $class_name );

        switch ( $type )
        {

            case MVC_TYPE_MODEL:
                return $this->models->get( $class_name );
                break;
            case MVC_TYPE_VIEW:
                return $this->views->get( $class_name );
                break;
            case MVC_TYPE_CONTROLLER:
                return $this->controllers->get( $class_name );
                break;
        }

        return null;
    }

    /**
     * @param string $type
     * @param string $class_name
     * @return null|void
     */

    public function remove( string $type, string $class_name )
    {

        $class_name = strtolower( $class_name );

        switch ( $type )
        {

            case MVC_TYPE_MODEL:
                $this->models->remove( $class_name );
                break;
            case MVC_TYPE_VIEW:
                $this->views->remove( $class_name );
                break;
            case MVC_TYPE_CONTROLLER:
                $this->controllers->remove( $class_name );
                break;
        }
    }

    /**
     * @param string $type
     * @return Constructor|null
     */

    public function getObject( string $type )
    {
        switch ( $type )
        {

            case MVC_TYPE_MODEL:
                return $this->models;
                break;
            case MVC_TYPE_VIEW:
                return $this->views;
                break;
            case MVC_TYPE_CONTROLLER:
                return $this->controllers;
                break;
        }

        return null;
    }

    /**
     * @param string $type
     * @return null|\stdClass
     */

    public function getClasses( string $type )
    {
        switch ( $type )
        {

            case MVC_TYPE_MODEL:
                return $this->models->getAll();
                break;
            case MVC_TYPE_VIEW:
                return $this->views->getAll();
                break;
            case MVC_TYPE_CONTROLLER:
                return $this->controllers->getAll();
                break;
        }

        return null;
    }

    /**
     * @throws \RuntimeException
     */

    public function initialize()
    {

        Debug::message('Initializing front controller');

        $this->models = new Constructor( $this->getFilepath(MVC_TYPE_MODEL), $this->getNamespace(MVC_TYPE_MODEL) );
        $this->views = new Constructor( $this->getFilepath(MVC_TYPE_VIEW), $this->getNamespace(MVC_TYPE_VIEW) );
        $this->controllers = new Constructor( $this->getFilepath(MVC_TYPE_CONTROLLER), $this->getNamespace(MVC_TYPE_CONTROLLER) );

        Debug::message( 'Creating instances of classes');

        $this->models->createAll();
        $this->views->createAll();
        $this->controllers->createAll();

        Debug::message('Finished front controller setup');
    }

    /**
     * @return bool
     */

    public function test()
    {

        if( empty( $this->models->getAll() ) )
        {

            return false;
        }

        if( empty( $this->views->getAll() ) )
        {

            return false;
        }

        if( empty( $this->controllers->getAll() ) )
        {

            return false;
        }

        return true;
    }

    /**
     * @param string $type
     * @return null|string
     */

    private function getFilepath( string $type )
    {

        switch( $type )
        {

            case MVC_TYPE_MODEL:
                return $this->filepath . MVC_NAMESPACE_MODELS . '/';
                break;
            case MVC_TYPE_VIEW:
                return $this->filepath . MVC_NAMESPACE_VIEWS . '/';
                break;
            case MVC_TYPE_CONTROLLER:
                return $this->filepath . MVC_NAMESPACE_CONTROLLERS . '/';
                break;
        }

        return null;
    }

    /**
     * @param string $type
     * @return null|string
     */

    private function getNamespace( string $type )
    {

        switch ( $type )
        {

            case MVC_TYPE_MODEL:
                return $this->namespace . MVC_NAMESPACE_MODELS . "\\";
                break;
            case MVC_TYPE_VIEW:
                return $this->namespace . MVC_NAMESPACE_VIEWS . "\\";
                break;
            case MVC_TYPE_CONTROLLER:
                return $this->namespace . MVC_NAMESPACE_CONTROLLERS . "\\";
                break;

        }

        return null;
    }
}