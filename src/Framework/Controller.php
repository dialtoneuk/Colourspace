<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 14:58
 */

namespace Colourspace\Framework;

use Colourspace\Framework\Interfaces\Controller as ControllerInterface;
use Colourspace\Framework\Interfaces\Model;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Util\Debug;
use Colourspace\Container;
use Colourspace\Framework\Util\Format;
use flight\net\Request;

abstract class Controller implements ControllerInterface
{

    /**
     * @var Model
     */

    public $model;

    /**
     * @var Recaptcha
     */

    protected $recaptcha;

    /**
     * @param Model $model
     * @throws \RuntimeException
     */

    public function setModel(Model $model )
    {

        if( empty( $model ) )
            throw new \RuntimeException('Model is invalid');

        $this->model = $model;
    }


    /**
     * @param bool $post
     * @return int
     */

    public function getPage( $post=false ): int
    {

        if( $post )
            if( isset( $_POST["page"] ) && is_nan( (int)$_GET["page"] ) == false )
                return( (int)$_POST["page"] );

        if (isset($_GET["page"]) )
            return ((int)$_GET["page"]);

        return 0;
    }

    /**
     * @param array $page
     * @throws \RuntimeException
     */

    public function setPage( array $page )
    {

        $key = $page["key"];

        if( isset( $this->model->$key ) == false )
            throw new \RuntimeException("key not set in model, have you created the object first?");

        $this->model->page = $page;
    }

    public function hasParams( Request $request )
    {

        if( isset( $request->params ) == false )
            return false;

        return true;
    }

    /**
     * @param Request $request
     * @param bool $object
     * @return object|array
     */

    public function getParams( Request $request, $object=true )
    {

        if (empty($request->params))
            throw new \RuntimeException("no params");

        if( $object )
            return( Format::toObject( $request->params ) );

        return( $request->params );
    }

    /**
     * @return array
     */

    public function keyRequirements()
    {

        return [];
    }

    /**
     * @throws \RuntimeException
     */

    public function before()
    {

        if( GOOGLE_RECAPTCHA_ENABLED )
            $this->recaptcha = Collector::new("Recaptcha");

        if( DEBUG_ENABLED )
            Debug::message("Controller initiating process method");
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     */

    public function process(string $type, Request $request)
    {

        switch ( $type )
        {

            case MVC_REQUEST_POST:
                print_r( $request );
                break;
            case MVC_REQUEST_GET:
                print_r( $request );
                break;
            case MVC_REQUEST_PUT:
                print_r( $request);
                break;
            case MVC_REQUEST_DELETE:
                print_r( $request);
                break;
        }

        return true;
    }

    /**
     * @param string $type
     * @param Request $request
     * @return bool
     * @throws \RuntimeException
     */

    public function authentication(string $type, Request $request)
    {

        Debug::message("Authentication called in base class: " . __CLASS__ );

        if ( Container::exist('application') == false )
            throw new \RuntimeException('Application has not been initialized');

        $application = Container::get('application');

        if( $application->session->isLoggedIn() )
            return true;

        return false;
    }

    /**
     * @return int|null
     * @throws \RuntimeException
     */

    public function userid()
    {

        if( Container::get("application")->session->isLoggedIn() == false )
            return null;

        return( Container::get('application')->session->userid() );
    }

    /**
     * @param $data
     * @param bool $empty_check
     * @return bool
     * @throws \RuntimeException
     */

    public function check( $data, $empty_check=true )
    {

        if( empty( $this->keyRequirements() ) )
            return true;

        foreach ( $this->keyRequirements() as $requirement )
        {

            if( is_string( $requirement ) == false )
                throw new \RuntimeException("Invalid type for requirement");

            if( isset( $data[ $requirement ] ) == false )
                return false;

            if( empty( $data[ $requirement ] ) && $empty_check )
                return false;
        }

        return true;
    }

    /**
     * @param $form
     * @return bool
     * @throws \Exception
     */

    public function checkRecaptcha( $form )
    {

        if( GOOGLE_RECAPTCHA_ENABLED )
        {

            return ( $this->recaptcha->isValid( $form->recaptcha ) );
        }
        else
            return true;
    }

    public function addRecaptcha()
    {

        if (GOOGLE_RECAPTCHA_ENABLED)
            $this->model->recaptcha = [
                'script' => $this->recaptcha->script(),
                'html' => $this->recaptcha->html()
            ];
    }

    /**
     * @param $data
     * @param bool $object
     * @param bool $escape
     * @return array|null|\stdClass
     */

    public function pickKeys( $data, $object=true, $escape=true )
    {

        if( empty( $this->keyRequirements() ) )
            return null;

        if( $object == false )
            $result = [];
        else
            $result = new \stdClass();

        foreach( $this->keyRequirements() as $requirement ) {

            if( $requirement == "g-recaptcha-response")
                $header = "recaptcha";
            else
                $header = $requirement;

            if( $escape )
                $data[ $requirement] = htmlspecialchars( $data[ $requirement ] );

            if( $object )
                $result->$header = $data[ $requirement ];
            else
                $result[ $header ] = $data[ $requirement ];
        }

        return $result;
    }
}