<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 03/07/2018
 * Time: 16:25
 */

namespace Colourspace\Framework;

use Colourspace\Container;
use Colourspace\Database\Tables\TemporaryUsernames;
use Colourspace\Framework\Util\Format;

class TemporaryUsername
{

    /**
     * @var TemporaryUsernames
     */

    protected $table;

    /**
     * @var Session
     */

    protected $session;

    /**
     * TemporaryUsername constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        if( Container::exist('application') == false )
            throw new \RuntimeException('Application has not been initialized');

        $this->table = new TemporaryUsernames();
        $this->session = Container::get('application')->session;
    }

    /**
     * @param $sessionid
     * @return bool
     */

    public function exist( $sessionid )
    {

        if( $this->table->exist( $sessionid ) == false )
            return false;

        return true;
    }

    /**
     * @param $sessionid
     * @return \Illuminate\Support\Collection
     */

    public function get( $sessionid )
    {

        return( $this->table->get( $sessionid )->first() );
    }

    /**
     * @param $sessionid
     */

    public function delete( $sessionid )
    {

        return( $this->table->remove( $sessionid ) );
    }

    /**
     * @param null $sessionid
     * @param null $username
     * @param $ipaddress
     * @return array
     * @throws \RuntimeException
     */

    public function add( $sessionid=null, $username=null, $ipaddress=null )
    {

        if( $sessionid == null )
        {

            if( $this->session->isActive() == false )
                throw new \RuntimeException('cannot predertermin session id as session is invalid');

            $sessionid = session_id();
        }

        if( $username == null )
            $username = $this->generate();

        if( $ipaddress == null )
            $ipaddress = $_SERVER["REMOTE_ADDR"];

        if( $this->exist( $sessionid ) )
            return null;

        $this->table->insert([
            "sessionid" => $sessionid,
            "username"  => $username,
            "ipaddress" => $ipaddress,
            "creation"  => Format::timestamp()
        ]);

        return([
            "sessionid" => $sessionid,
            "username"  => $username,
            "ipaddress" => $ipaddress,
            "creation"  => Format::timestamp()
        ]);
    }

    /**
     * @return string
     */

    public function generate()
    {

        $username = ACCOUNT_PREFIX;
        $digits = "";

        for( $i = 0; $i < ACCOUNT_DIGITS; $i++ )
        {

            $digits = $digits . rand( ACCOUNT_RND_MIN, ACCOUNT_RND_MAX );
        }

        return( $username . $digits );
    }
}