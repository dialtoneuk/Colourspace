<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 14/07/2018
 * Time: 22:29
 */

namespace Colourspace\Framework;

use Colourspace\Database\Tables\Verifications as Table;
use Colourspace\Framework\Util\Format;

class Verifications
{

    /**
     * @var Table
     */

    protected $table;

    /**
     * Verifications constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->table = new Table();
    }

    /**
     * @param int $userid
     * @return bool
     */#

    public function userHas( int $userid )
    {

        if( $this->table->search( "userid", $userid )->isEmpty() )
            return false;

        return true;
    }

    /**
     * @param int $userid
     * @param string $type
     * @param string|null $token
     * @return array
     * @throws \RuntimeException
     * @throws \Exception
     */

    public function create( int $userid, string $type, string $token=null )
    {

        if( $this->hasType( $userid, $type ) )
            throw new \RuntimeException("User already has type: " . $type );

        if( $token == null )
            $token = $this->getToken();

        $id = $this->table->insert([
            'userid' => $userid,
            'type'  => $type,
            'data'  => Format::largeText( json_encode([
                'token' => $token,
                "created" => time()
            ]))
        ]);

        return([
            'id' => $id,
            'token' => $token
        ]);
    }

    /**
     * @param int $verifyid
     * @return bool
     */

    public function exist( int $verifyid )
    {

        return( $this->table->exist( $verifyid ) );
    }

    /**
     * @param int $userid
     * @param bool $first
     * @return \Illuminate\Support\Collection|mixed
     */

    public function user( int $userid, $first=false )
    {

        if( $first )
            return( $this->table->user( $userid )->first() );

        return( $this->table->user( $userid ) );
    }

    /**
     * @param $userid
     * @param $type
     * @return \Illuminate\Support\Collection
     */

    public function type( $userid, $type )
    {

        return( $this->table->query()->where([ COLUMN_USERID => $userid, "type" => $type ])->get() );
    }

    /**
     * @param $userid
     * @param $type
     * @return bool
     */

    public function hasType( $userid, $type )
    {

        if(  $this->type( $userid, $type )->isEmpty() )
            return false;

        return true;
    }

    /**
     * @param int $verifyid
     * @param string $token
     * @return bool
     */

    public function check( int $verifyid, string $token )
    {

        if( $this->exist( $verifyid ) == false )
            return false;

        $verify = $this->get( $verifyid );
        $data = json_decode( Format::decodeLargeText( $verify->data ) );

        if( $data->created > time() * 60 * 60 * 2 )
            return false;

        if( $data->token != $token )
            return false;

        return true;
    }

    /**
     * @param $userid
     */

    public function clear( $userid )
    {

        $this->table->cleanse("userid", $userid );
    }

    /**
     * @param $verifyid
     * @return mixed
     */

    public function get( $verifyid )
    {

        return( $this->table->get( $verifyid )->first() );
    }

    /**
     * @param $key
     * @return array
     * @throws \RuntimeException
     */

    public function parse( string $key )
    {

        $key = Format::decodeLargeText( $key );

        if( str_contains( $key, ":") == false )
            throw new \RuntimeException("Key is invalid");

        $result = explode( ":", $key );

        if( count( $result ) !== 2 )
            throw new \RuntimeException("Key is invalid");

        return( $result );
    }

    /**
     * @param $verifyid
     */

    public function delete( $verifyid )
    {

        $this->table->remove( $verifyid );
    }

    /**
     * @param array $data
     * @return string
     */

    public function format( array $data )
    {

        return( Format::largeText( $data["id"] . ":" . $data["token"] ) );
    }

    /**
     * @return string
     * @throws \Exception
     */

    private function getToken()
    {

        return( base64_encode( @random_bytes(16) ) );
    }
}