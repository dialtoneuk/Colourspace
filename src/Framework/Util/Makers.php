<?php

namespace Colourspace\Framework\Util;
use Colourspace\Framework\Util\Conventions\FileData;
use Colourspace\Framework\Util\Conventions\TokenData;
use Colourspace\Framework\Util\Interfaces\Maker;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 21:43
 */

class Makers extends Collection
{

    /**
     * Makers constructor.
     * @param string $filepath
     * @param string $namespace
     * @param bool $auto_create
     */

    public function __construct($filepath="src/Framework/Util/Makers/", $namespace="Colourspace\\Framework\\Util\\Makers\\", bool $auto_create = true)
    {

        parent::__construct($filepath, $namespace, $auto_create);
    }

    /**
     * @param TokenData $values
     * @param string $class_name
     * @param $path
     * @param FileData $template
     * @return FileData
     * @throws \Exception
     */

    public function process( TokenData $values, $class_name="Constructor", $path, FileData $template = null )
    {

        if( $this->exist( $class_name ) == false )
            throw new \RuntimeException("class does not exist");

        $instance = $this->constructor->get( $class_name );

        /** @var Maker $instance */
        if( $instance instanceof Maker == false )
            throw new \RuntimeException("invalid instance");

        try
        {

            $instance->before( $template );

            if( empty( $instance->requiredTokens() ) == false )
                if( $this->checkTokenData( $values, $instance->requiredTokens() ) == false )
                    throw new \RuntimeException("invalid token data");

            return( $instance->make( $values, $path ) );

        }
        catch ( \Exception $exception )
        {

            throw $exception;
        }
    }

    /**
     * @param $class_name
     * @return array
     */

    public function getRequiredTokens( $class_name )
    {


        if( $this->exist( $class_name ) == false )
            throw new \RuntimeException("class does not exist");

        $instance = $this->constructor->get( $class_name );

        /** @var Maker $instance */
        if( $instance instanceof Maker == false )
            throw new \RuntimeException("invalid instance");

        return( $instance->requiredTokens() );
    }

    /**
     * @param TokenData $values
     * @param $requirements
     * @return bool
     */

    private function checkTokenData( TokenData $values, $requirements )
    {

        foreach( $requirements as $key=>$value )
            if( isset( $values->values[ $value ] ) == false )
                return false;

        return true;
    }
}