<?php

namespace Colourspace\Framework\Util\Makers;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 22:11
 */

use Colourspace\Framework\Util\Conventions\FileData;
use Colourspace\Framework\Util\FileOperator;

class Convention extends Base
{

    public function before(FileData $template = null): void
    {

        if( $template == null )
            $template = FileOperator::pathDataInstance("resources/templates/template_convention.module");

        parent::before($template);
    }
}