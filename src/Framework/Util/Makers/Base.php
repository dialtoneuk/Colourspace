<?php

namespace Colourspace\Framework\Util\Makers;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 21:48
 */

use Colourspace\Framework\Util\Conventions\FileData;
use Colourspace\Framework\Util\Conventions\TokenData;
use Colourspace\Framework\Util\Interfaces\Maker;
use Colourspace\Framework\Util\TokenReader;

abstract class Base implements Maker
{

    /**
     * @var FileData
     */

    protected $template;

    /**
     * @var TokenReader
     */

    protected $tokenreader;

    /**
     * Base constructor.
     */

    public function __construct()
    {

        $this->tokenreader = new TokenReader();
    }

    /**
     * @param FileData $template
     */

    public function before(FileData $template=null): void
    {

        if( $template == null )
            throw new \RuntimeException("template is null and has not been set by inheritor");

        if( $this->exist( $template->path ) == false )
            throw new \RuntimeException("path does not exist: " . $template->path );

        $this->template = $template;
    }

    /**
     * @return array
     */

    public function requiredTokens(): array
    {

        return(["classname","namespace"]);
    }

    /**
     * @param TokenData $values
     * @param $path
     * @return FileData|bool
     */

    public function make(TokenData $values, $path ): FileData
    {

        if( count(  explode(".", $path ) ) == 1 )
            $path = $path . $values->arrayValue("values", "classname") . ".php";

        $result = $this->tokenreader->parse( $this->template, $values, $path );

        if( $result == false )
            if( TokenReader::hasLastError() )
                throw TokenReader::getLastError();

        return( $result );
    }

    /**
     * @param $path
     * @return bool
     */

    private function exist( $path )
    {

        return( file_exists( COLOURSPACE_ROOT . $path ) );
    }
}