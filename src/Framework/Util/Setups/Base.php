<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 21/07/2018
 * Time: 03:06
 */

namespace Colourspace\Framework\Util\Setups;


use Colourspace\Framework\Util\Interfaces\Setup;
use Colourspace\Framework\Util\Debug;

abstract class Base implements Setup
{

    /**
     * Base constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        if( Debug::isCMD() == false )
            throw new \RuntimeException("Not in CMD mode");
    }

    /**
     * @return bool
     */

    public function process()
    {

        return( true );
    }

    /**
     * @param array $inputs
     * @return array
     */

    public function getInputs( array $inputs )
    {

        $results = [];

        foreach( $inputs as $key=>$value )
            $results[ $key ] = Debug::getLine( $key );

        return( $results );
    }

    /**
     * @param $file
     * @param bool $array
     * @return mixed
     */

    public function read( $file, $array=true )
    {

        return( json_decode( file_get_contents( COLOURSPACE_ROOT . $file ), $array ) );
    }

    /**
     * @param $file
     * @param array $data
     */

    public function write( $file, array $data )
    {

        file_put_contents( COLOURSPACE_ROOT . $file, json_encode( $data ) );
    }

    /**
     * @param $file
     * @return bool
     */

    public function exists( $file )
    {

        return( file_exists( COLOURSPACE_ROOT . $file ) );
    }
}