<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 21/07/2018
 * Time: 03:26
 */

namespace Colourspace\Framework\Util\Setups;


class Google extends Base
{

    /**
     * Aws constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        if( $this->exists( GOOGLE_RECAPTCHA_CREDENTIALS ) == false )
            throw new \RuntimeException("File does not exist");

        parent::__construct();
    }

    /**
     * @return bool
     */

    public function process()
    {

        $inputs = $this->getInputs([
            "key",
            "secret"
        ]);

        $this->write( GOOGLE_RECAPTCHA_CREDENTIALS, $inputs );

        return parent::process();
    }
}