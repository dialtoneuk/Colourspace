<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/08/2018
 * Time: 03:00
 */

namespace Colourspace\Framework\Util\Migrators;


use Colourspace\Framework\Util\Interfaces\Migrator;

abstract class Base implements Migrator
{

    /**
     * @return mixed
     */

    public function migrate()
    {

        return( true );
    }
}