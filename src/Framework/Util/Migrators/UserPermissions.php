<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 20/07/2018
 * Time: 20:20
 */

namespace Colourspace\Framework\Util\Migrators;


class UserPermissions extends Base
{

    /**
     * @throws \RuntimeException
     */

    public function migrate()
    {

        if( file_exists( COLOURSPACE_ROOT . USER_PERMISSIONS_ROOT ) == false )
            mkdir( COLOURSPACE_ROOT . USER_PERMISSIONS_ROOT );

        file_put_contents( COLOURSPACE_ROOT . USER_PERMISSIONS_ROOT . "config.json", json_encode([
            "created" => time()
        ]));
    }
}