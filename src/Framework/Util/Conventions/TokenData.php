<?php

namespace Colourspace\Framework\Util\Conventions;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 21:44
 */

use Colourspace\Framework\Util\Convention;
use Colourspace\Framework\Util\Format;

/**
 * Class TokenData
 * @package Colourspace\Framework\Util\Conventions
 * @property array values
 */

class TokenData extends Convention
{

    /**
     * @var array
     */

    protected $requirements = [
        "values" => "array"
    ];

    /**
     * TokenData constructor.
     * @param array $array
     */

    public function __construct(array $array)
    {

        if( isset( $array["values"]["time"] ) == false )
            $array["values"]["time"] = Format::timestamp( time() );

        parent::__construct($array);
    }
}