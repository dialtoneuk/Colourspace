<?php

namespace Colourspace\Framework\Util\Conventions;


/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 01:16
 */

use Colourspace\Framework\Util\Convention;

/**
 * Class UploadData
 * @package Colourspace\Framework\Util\Conventions
 *
 * @property string filename
 * @property array settings
 * @property array form
 */

class UploadData extends Convention
{

    /**
     * @var array
     */

    protected $requirements = [
        "filename" => "string",
        "settings" => "array",
        "form"     => "array"
    ];
}