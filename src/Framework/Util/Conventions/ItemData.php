<?php

namespace Colourspace\Framework\Util\Conventions;


/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 01:16
 */

use Colourspace\Framework\Util\Convention;

/**
 * Class UploadData
 * @package Colourspace\Framework\Util\Conventions
 *
 * @property string name
 * @property string item
 * @property string description
 * @property int cost
 * @property array requirements
 * @property bool onetime
 * @property bool discontinued
 */

class ItemData extends Convention
{

    /**
     * @var array
     */

    protected $requirements = [
        "name"          => "string",
        "item"          => "string",
        "description"   => "string",
        "cost"          => "int",
        "requirements"  => "array",
        "onetime"       => "bool",
        "discontinued"  => "bool"
    ];
}