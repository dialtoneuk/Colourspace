<?php

namespace Colourspace\Framework\Util\Conventions;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 21:46
 */

use Colourspace\Framework\Util\Convention;

/**
 * Class FileData
 * @package Colourspace\Framework\Util\Conventions
 * @property string path
 * @property string contents
 * @property array info
 */

class FileData extends Convention
{

    /**
     * @var array
     */

    protected $requirements = [
        "path" => "string",
        "contents" => "string",
        "info" => "array"
    ];
}