<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/08/2018
 * Time: 14:16
 */

namespace Colourspace\Framework\Util\Scripts;


use Colourspace\Framework\Util\Debug;

class JsonReader extends Base
{

    /**
     * @param $arguments
     * @return bool
     * @throws \RuntimeException
     */

    public function execute($arguments)
    {

        if( count( explode(".", $arguments["file"]  ) ) == 1 )
            $arguments["file"] = $arguments["file"] . ".json";

        if( file_exists( COLOURSPACE_ROOT . "config/" . $arguments["file"] ) == false )
            throw new \RuntimeException("File does not exist");

        if( Debug::isCMD() )
            Debug::echo( "Opening file: " . $arguments["file"], 1 );

        $contents = file_get_contents( COLOURSPACE_ROOT . "config/" . $arguments["file"] );

        if( Debug::isCMD() )
            Debug::echo( $contents );

        return true;
    }

    /**
     * @return array|null
     */

    public function requiredArguments()
    {

        return([
            "file"
        ]);
    }
}