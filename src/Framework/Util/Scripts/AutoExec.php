<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 06/08/2018
 * Time: 15:02
 */

namespace Colourspace\Framework\Util\Scripts;

use Colourspace\Framework\Util\AutoExec as AutoExecManager;
use Colourspace\Container;
use Colourspace\Framework\Util\Debug;

class AutoExec extends Base
{

    /**
     * @var AutoExecManager
     */

    protected $autoexecmanager;

    /**
     * AutoExec constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->autoexecmanager = new AutoExecManager( false );
    }

    /**
     * @param $arguments
     * @return bool
     * @throws \RuntimeException
     */

    public function execute($arguments)
    {

        if( Container::exist("application") == false )
            $this->initContainer();

        $this->autoexecmanager->create();

        if( $this->autoexecmanager->exist( $arguments["script"] ) == false )
            throw new \RuntimeException("Script does not exist: " . $arguments["script"]);

        if( Debug::isCMD() )
            Debug::echo("Executing autoexec script: " . $arguments["script"], 5 );

        if( $arguments["arguments"] == "false" || $arguments["arguments"] == "f" ||  $arguments["arguments"] == null )
            $this->autoexecmanager->execute( $arguments["script"] );
        else
        {

            $data = $this->parse( $arguments["arguments"] );
            $this->autoexecmanager->execute( $arguments["script"], $data );
        }

        if( Debug::isCMD() )
            Debug::echo("Finished: " . $arguments["script"], 5 );

        return parent::execute($arguments); // TODO: Change the autogenerated stub
    }

    /**
     * @return array|null
     */

    public function requiredArguments()
    {

        return([
            "script",
            "arguments"
        ]);
    }
}