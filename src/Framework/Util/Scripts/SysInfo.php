<?php

namespace Colourspace\Framework\Util\Scripts;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 18:10
 */

use Colourspace\Container;
use Colourspace\Framework\Util\Debug;

class SysInfo extends Base
{

    /**
     * @var array
     */

    protected $info;

    /**
     * SysInfo constructor.
     */

    public function __construct()
    {

        $this->info = $this->default();
    }

    /**
     * @param $arguments
     * @return bool
     */

    public function execute($arguments)
    {

        //Push some lines onto the stack which are only excessible when we execute
        array_push( $this->info, " loaded scripts: " . count( Container::get("scripts")->scripts() ) );

        $keys = array_keys( $arguments );

        if( isset( $arguments["detailed"] ) == false )
        {
            //Cute little hack to allow easier use with help
            $keys = array_keys( $arguments );

            if( empty( $keys ) == false )
                if( $keys[0] == "detailed" )
                    $arguments["detailed"] = true;
        }


        if( isset( $arguments["detailed"] ) && $arguments["detailed"] == true )
        {

            array_push( $this->info, "\nConstants\n");
            array_push( $this->info, get_defined_constants() );
        }
        //Check for trailing new line if not add one
        if( last( $this->info ) !== "\n" && array_has( $this->info, "\n") == false )
            array_push( $this->info, "\n");

        foreach ( $this->info as $item )
        {

            if( is_string( $item ) == false )
                $string = print_r( $item);

            if( Debug::isSuppressed() )
                echo( $item . "\n" );
            else
                Debug::echo( $item);
        }

        $this->info = $this->default();

        return parent::execute($arguments); // TODO: Change the autogenerated stub
    }

    /**
     * @return array|null
     */

    public function requiredArguments()
    {

        return parent::requiredArguments();
    }

    /**
     * @return array
     */

    public function help()
    {

        return([
            "arguments" => $this->requiredArguments(),
            "help" => "Execute sysinfo detailed in order to view more detailed information."
        ]);
    }

    /**
     * @return array
     */

    private function default()
    {

        return([
            "[Colourspace CLI Instance]",
            "Written by Lewis Lancaster (31/08/2018) in PHP 7\n",
            "Server Info:\n",
            " php version: " . PHP_VERSION,
            " sapi: " . php_sapi_name(),
            " cwd: " . getcwd(),
            " colourspace root: " . COLOURSPACE_ROOT
        ]);
    }
}