<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 22/07/2018
 * Time: 01:01
 */

namespace Colourspace\Framework\Util\Scripts;

use Colourspace\Framework\Util\Debug;
use Colourspace\Framework\Util\ResourceCombiner;
use Colourspace\Framework\Util\ResourceUnpacker;

class Resources extends Base
{

    /**
     * @var ResourceUnpacker
     */

    protected $unpacker;

    /**
     * @var ResourceCombiner
     */

    protected $packer;

    /**
     * Resources constructor.
     * @throws \RuntimeException
     */
    
    public function __construct()
    {
        
        $this->unpacker = new ResourceUnpacker();
        $this->packer = new ResourceCombiner();
    }

    /**
     * @param $arguments
     * @return bool
     * @throws \RuntimeException
     */

    public function execute($arguments)
    {

        if( $arguments["action"] == "pack" )
        {

            if( Debug::isCMD() )
                Debug::echo("Packing resources", 3);

            $build = $this->packer->build();

            if( empty( $build ) )
                throw new \RuntimeException("Build returned null ");

            $this->packer->save( $build );

            if( Debug::isCMD() )
                Debug::echo("Finished Packing resources", 3);
        }
        elseif( $arguments["action"] == "unpack")
        {

            if( Debug::isCMD() )
                Debug::echo("Unpacking resources", 3);

            $this->unpacker->process();
        }
        else
            throw new \RuntimeException("Unknown action");

        return( true );
    }

    /**
     * @return array
     */

    public function requiredArguments()
    {

        return([
            "action"
        ]);
    }


    /**
     * @return array
     */

    public function help()
    {
        return([
            "arguments" => $this->requiredArguments(),
            "help" => "Unpacks and packs resources. action can either be pack or unpack."
        ]);
    }
}