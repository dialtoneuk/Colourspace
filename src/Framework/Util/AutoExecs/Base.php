<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 06/08/2018
 * Time: 01:50
 */

namespace Colourspace\Framework\Util\AutoExecs;


use Colourspace\Container;
use Colourspace\Framework\Util\Interfaces\AutoExec;

abstract class Base implements AutoExec
{

    /**
     * @var \Colourspace\Framework\Session
     */

    protected $session;

    /**
     * Base constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        if( Container::exist("application") == false )
            throw new \RuntimeException("Needs application");

        $this->session = Container::get("application")->session;
    }

    /**
     * @param array $data
     * @return mixed|void
     */

    public function execute( array $data )
    {

        return;
    }
}