<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 20/07/2018
 * Time: 18:54
 */

namespace Colourspace\Framework\Util\Interfaces;


interface Setup
{

    public function process();
}