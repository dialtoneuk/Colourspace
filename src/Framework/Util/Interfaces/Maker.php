<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 21:43
 */

namespace Colourspace\Framework\Util\Interfaces;


use Colourspace\Framework\Util\Conventions\FileData;
use Colourspace\Framework\Util\Conventions\TokenData;

interface Maker
{

    public function before( FileData $template=null ): void;

    public function requiredTokens(): array;

    public function make( TokenData $values, $path ): FileData;
}