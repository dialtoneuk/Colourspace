<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/08/2018
 * Time: 02:14
 */

namespace Colourspace\Framework\Util\Interfaces;


interface Response
{

    public function get();
}