<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/08/2018
 * Time: 21:42
 */

namespace Colourspace\Framework\Util\Interfaces;


interface Test
{

    /**
     * @return bool
     */

    public function execute();
}