<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 16:49
 */

namespace Colourspace\Framework\Models;

use Colourspace\Framework\Model;

class Normal extends Model
{

    /**
     * @throws \RuntimeException
     */

    public function startup()
    {

        $this->setProfiles([
            "Session"           => [],
            "User"              => ["loggedin" => true],
            "Group"             => ["loggedin" => true],
            "UserPermissions"   => ["loggedin" => true],
            "Balance"           => ["loggedin" => true ]
        ]);

        //Remember to call start up to process the profiles
        parent::startup();
    }
}