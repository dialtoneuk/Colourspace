<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 03/07/2018
 * Time: 16:41
 */

namespace Colourspace\Framework\Models;




class Register extends Normal
{

    /**
     * @throws \RuntimeException
     */

    public function startup()
    {

        $this->setProfiles([
            "Temporary" => ["loggedin" => true]
        ]);

        //Remember to call start up to process the profiles
        parent::startup();
    }
}