<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 13/08/2018
 * Time: 02:02
 */

namespace Colourspace\Framework\Models;


use Colourspace\Container;
use Colourspace\Framework\Shop;

class Store extends Normal
{

    /**
     * @throws \RuntimeException
     */
    
    public function startup()
    {
        
        if( Container::get("application")->session->isLoggedIn() )
        {

            $shop = new Shop();

            $this->object->items = $shop->inventory();

            $userid = Container::get("application")->session->userid();

            if( $shop->hasTransactions( $userid ) )
                foreach( $this->object->items as $key=>$item )
                    if( $shop->hasTransactionItem( $userid, $item["item"] ) )
                        $this->object->items[ $key ]["purchased"] = true;
                    else
                        $this->object->items[ $key ]["purchased"] = false;
        }

        //Remember to call start up to process the profiles
        parent::startup();
    }
}