<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 16:49
 */

namespace Colourspace\Framework\Models;



class Debug extends Normal
{

    /**
     * @throws \RuntimeException
     */

    public function startup()
    {

        $this->setProfiles([
            "Verifications"     => ["loggedin" => true],
            "Followers"         => []
        ]);

        //Remember to call start up to process the profiles
        parent::startup();
    }
}