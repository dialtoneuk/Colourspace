<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/08/2018
 * Time: 23:11
 */

namespace Colourspace\Framework;


use Colourspace\Database\Tables\Audits;
use Colourspace\Framework\Util\Format;


class Moderation
{

    /**
     * @var Audits
     */

    protected $audits;

    /**
     * Moderation constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->audits = new Audits();
    }

    /**
     * @param $admin
     * @return \Illuminate\Support\Collection
     */

    public function getActions( $admin )
    {

        return( $this->audits->search("admin", $admin ) );
    }

    /**
     * @param $auditid
     * @return \Illuminate\Support\Collection
     */

    public function audit( $auditid )
    {

        return( $this->audits->get( $auditid ) );
    }

    /**
     * @param $auditid
     * @param $values
     */

    public function update( $auditid, $values )
    {

        $this->audits->update(["auditid" => $auditid], $values );
    }

    /**
     * @param $userid
     * @return \Illuminate\Support\Collection
     */

    public function getAudits( $userid )
    {

        return( $this->audits->user( $userid ) );
    }

    /**
     * @param $auditid
     * @return bool
     */

    public function exist( $auditid )
    {

        return( $this->audits->exist( $auditid ) );
    }

    /**
     * @param $userid
     * @return bool
     */

    public function isBanned( $userid )
    {

        if( $this->has( AUDIT_TYPE_BAN, $userid ) == false )
            return false;

        return true;
    }

    /**
     * @param $userid
     * @return mixed
     */

    public function getBan( $userid )
    {

        return( $this->get( AUDIT_TYPE_BAN, $userid )->first() );
    }

    /**
     * @param $userid
     * @return mixed
     * @throws \RuntimeException
     */

    public function getBanMetainfo( $userid )
    {

        $ban = $this->getBan( $userid );

        if( isset( $ban->metainfo ) == false )
            throw new \RuntimeException("Invalid ban return");

        return( json_decode( $ban->metainfo ) );
    }

    /**
     * @param $type
     * @return \Illuminate\Support\Collection
     */

    public function all( $type )
    {

        return( $this->audits->search("type", $type) );
    }

    /**
     * @param $auditid
     * @return bool
     * @throws \RuntimeException
     */

    public function canUnban( $auditid )
    {

        $audit = $this->audit( $auditid );

        if( $audit->type !== AUDIT_TYPE_BAN )
            throw new \RuntimeException("Invalid audit type, is not a ban type");

        $metainfo = json_decode( $audit->metainfo );

        if( $metainfo->expires > time() )
            return true;

        return false;
    }

    /**
     * @param $type
     * @param $userid
     * @param $admin
     * @param array $metainfo
     * @return int
     * @throws \RuntimeException
     */

    public function create( $type, $userid, $admin, array $metainfo )
    {

        return( $this->audits->insert([
            "type"      => $type,
            "userid"    => $userid,
            "admin"     => $admin,
            "metainfo"  => Format::toJson( $metainfo ),
            "creation"  => Format::timestamp()
        ]));
    }

    /**
     * @param $reason
     * @param int $effective
     * @param int $expires
     * @return array
     */

    public function metainfoBan( $reason, int $effective, int $expires )
    {

        return([
            "reason"    => $reason,
            "effective" => Format::timestamp( $effective ),
            "expires"   => Format::timestamp( $expires )
        ]);
    }

    /**
     * @param $reason
     * @param int|null $effective
     * @return array
     */

    public function metainfoWarned( $reason, int $effective = null )
    {

        if( $effective == null )
            $effective = time();

        return([
            "reason"    => $reason,
            "effective" => Format::timestamp( $effective )
        ]);
    }

    /**
     * @param $group
     * @param int|null $effective
     * @return array
     */

    public function metainfoGroupChange( $group, int $effective = null )
    {

        if( $effective == null )
            $effective = time();

        return([
            "newgroup"    => $group,
            "effective" => Format::timestamp( $effective )
        ]);
    }

    /**
     * @param $auditid
     */

    public function remove( $auditid )
    {

        $this->audits->remove( $auditid );
    }

    /**
     * @param $type
     * @param $userid
     * @return bool
     */

    public function has( $type, $userid )
    {

        if( $this->get( $type, $userid )->isEmpty() )
            return false;

        return true;
    }

    /**
     * @param $type
     * @param $userid
     * @return \Illuminate\Support\Collection
     */

    public function get( $type, $userid )
    {

        return( $this->audits->query()->where(["type" => $type, "userid" => $userid ] )->get() );
    }
}