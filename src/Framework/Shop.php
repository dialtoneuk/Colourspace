<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 18/07/2018
 * Time: 21:15
 */

namespace Colourspace\Framework;


use Colourspace\Database\Tables\Transactions;
use Colourspace\Framework\Interfaces\Item;
use Colourspace\Framework\Util\Collection;
use Colourspace\Framework\Util\Conventions\ItemData;
use Colourspace\Framework\Util\Format;


class Shop extends Collection
{

    /**
     * @var Transactions
     */

    protected $transactions;

    /**
     * @var array
     */

    protected $inventory;



    public function __construct($filepath=SHOP_ROOT, $namespace=SHOP_NAMESPACE, bool $auto_create = true)
    {

        if( $this->inventoryExists() == false )
            throw new \RuntimeException("Inventory does not exist");

        $this->transactions = new Transactions();
        $this->inventory = $this->inventory();

        parent::__construct($filepath, $namespace, $auto_create);
    }

    /**
     * @param $transactionid
     * @return mixed
     */

    public function transaction( $transactionid )
    {

        return( $this->transactions->get( $transactionid )->first() );
    }

    public function hasTransactions( $userid )
    {

        if( $this->transactions->query()->where([ COLUMN_USERID => $userid ] )->get()->isEmpty() )
            return false;

        return true;
    }

    /**
     * @param $userid
     * @return \Illuminate\Support\Collection
     */

    public function transactions( $userid )
    {

        return( $this->transactions->query()->where([ COLUMN_USERID => $userid ] )->get() );
    }

    /**
     * @param $transactionid
     * @return bool
     */

    public function transactionExists( $transactionid )
    {

        return( $this->transactions->exist( $transactionid ) );
    }

    /**
     * @param $userid
     * @param $class_name
     * @return \Illuminate\Support\Collection
     */

    public function transactionItem( $userid, $class_name )
    {

        return( $this->transactions->query()->where([ COLUMN_USERID => $userid, "item" => $class_name ] )->get() );
    }

    /**
     * @param $class_name
     * @return mixed
     * @throws \RuntimeException
     */

    public function cost( $class_name )
    {

        $item = $this->getInventoryItem( $class_name, false );

        if( $item == null )
            throw new \RuntimeException("Item does not exist");

        return( $item->cost );
    }

    /**
     * @param $class_name
     * @param bool $array
     * @return ItemData|null|array
     */

    public function getInventoryItem( $class_name, $array=true )
    {

        foreach( $this->inventory as $key=>$value )
        {

            if( strtolower( $value["item"] ) == strtolower( $class_name ) )
            {

                if( $array )
                    return( $value );

                return( self::dataInstance( $value ) );
            }
        }

        return null;
    }

    /**
     * @param $userid
     * @param $class_name
     * @param $value
     * @param null $type
     * @throws \RuntimeException
     */

    public function createTransaction( $userid, $class_name, $value, $type=null )
    {

        if( $type == null )
            $type = TRANSACTION_TYPE_WITHDRAW;

        $this->transactions->insert([
            "userid"    => $userid,
            "value"     => $value,
            "type"      => $type,
            "item"      => $class_name,
            "when"      => Format::timestamp()
        ], false );
    }

    /**
     * @param $userid
     * @param $class_name
     * @return bool
     */

    public function hasTransactionItem( $userid, $class_name )
    {

        return( $this->transactionItem( $userid, $class_name )->isNotEmpty() );
    }

    /**
     * @param $userid
     * @param $type
     * @return \Illuminate\Support\Collection
     */

    public function getType( $userid, $type )
    {

        return( $this->transactions->query()->where([ COLUMN_USERID => $userid, "type" => $type ] )->get() );
    }

    /**
     * @param $userid
     * @param $class_name
     * @param array $data
     * @return bool
     * @throws \RuntimeException
     */

    public function authenticate( $userid, $class_name, array $data )
    {

        if( $this->exist( $class_name ) == false )
            throw new \RuntimeException("Item does not exist");

        $item = $this->constructor->get( $class_name );

        if( $item instanceof Item == false )
            throw new \RuntimeException("Incorrect class returned");

        if( @$item->authentication( $userid, $data ) == false )
            return false;

        return true;
    }

    /**
     * @param $class_name
     * @return bool
     * @throws \RuntimeException
     */

    public function before( $class_name )
    {

        if( $this->exist( $class_name ) == false )
            throw new \RuntimeException("Item does not exist");

        $item = $this->constructor->get( $class_name );

        if( $item instanceof Item == false )
            throw new \RuntimeException("Incorrect class returned");

        $item->before();

        return true;
    }

    /**
     * @param $userid
     * @param $class_name
     * @param array $data
     * @return bool
     * @throws \RuntimeException
     */

    public function complete( $userid, $class_name, array $data )
    {

        if( $this->exist( $class_name ) == false )
            throw new \RuntimeException("Item does not exist");

        $item = $this->constructor->get( $class_name );

        if( $item instanceof Item == false )
            throw new \RuntimeException("Incorrect class returned");

        if( @$item->purchase( $userid, $data ) == false )
            return false;

        return true;
    }

    /**
     * @param $class_name
     * @param bool $check_inventory
     * @return bool
     */

    public function exist($class_name, $check_inventory=true )
    {

        if( $this->constructor->exist( $class_name ) == false )
            return false;

        if( $this->getInventoryItem( $class_name ) == null )
            return false;

        return true;
    }

    /**
     * @return array
     * @throws \RuntimeException
     */

    public function inventory()
    {

        if( $this->inventory == null || empty( $this->inventory ) )
            $this->inventory = json_decode( $this->read(), true );

        return( $this->inventory );
    }

    /**
     * @return bool
     */

    private function inventoryExists()
    {

        return( file_exists( COLOURSPACE_ROOT . SHOP_INVENTORY ) );
    }

    /**
     * @return bool|string
     */

    private function read()
    {

        return( file_get_contents( COLOURSPACE_ROOT . SHOP_INVENTORY ) );
    }

    /**
     * @param array $values
     * @return ItemData
     */

    public static function dataInstance( array $values )
    {

        return( new ItemData( $values ) );
    }
}