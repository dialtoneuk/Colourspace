<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/07/2018
 * Time: 20:16
 */

namespace Colourspace\Framework;


use Colourspace\Database\Tables\Tracks;
use Colourspace\Framework\Util\Format;

class Track
{

    protected $table;

    /**
     * Track constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->table = new Tracks();
    }

    /**
     * @param $trackid
     * @return bool
     */

    public function exists( $trackid )
    {

        return( $this->table->exist( $trackid ) );
    }

    /**
     * @param $trackid
     * @return mixed
     */

    public function get( $trackid )
    {

        return( $this->table->get( $trackid ) )->first();
    }

    /**
     * @param $trackname
     * @return \Illuminate\Support\Collection
     */

    public function find( $trackname )
    {

        return( $this->table->search( "trackname", $trackname ) );
    }

    /**
     * @param $trackid
     * @param bool $array
     * @return mixed
     * @throws \RuntimeException
     */

    public function metainfo( $trackid, $array=true )
    {

        $track = $this->get( $trackid );
        
        if( isset( $track->metainfo ) == false )
            throw new \RuntimeException("Invalid key");

        $json = Format::decodeLargeText( $track->metainfo );

        if( empty( $json ) )
            throw new \RuntimeException("Invalid metainfo");

        $json = json_decode( $json, $array );

        if( json_last_error() !== JSON_ERROR_NONE )
            throw new \RuntimeException("Json Error:" . json_last_error_msg() );

        return( $json );
    }

    /**
     * @param $trackid
     * @param $privacy
     * @throws \RuntimeException
     */

    public function updatePrivacy( $trackid, $privacy )
    {

        if( $privacy !== TRACK_PRIVACY_PERSONAL || $privacy !== TRACK_PRIVACY_PUBLIC || $privacy !== TRACK_PRIVACY_PRIVATE )
            throw new \RuntimeException("Invalid privacy type");

        $this->table->update(["trackid" => $trackid],["privacy" => $privacy ] );
    }

    /**
     * @param $knownas
     * @param $description
     * @param $credits
     * @param $waveform
     * @param $type
     * @return array
     * @throws \RuntimeException
     */

    public function getMetadataArray( $knownas, $description, $credits, $waveform, $type )
    {

        return([
            "aka"           => $knownas,
            "description"   => $description,
            "credits"       => $credits,
            "waveform"      => $waveform,
        ]);
    }

    /**
     * @param $trackid
     * @param array $values
     * @throws \RuntimeException
     */

    public function updateMetadata( $trackid, array $values )
    {

        $metainfo = $this->metainfo( $trackid, true );

        if( empty( $metainfo ) )
            throw new \RuntimeException("Invalid metainfo");

        foreach( $values as $key=>$value )
            $metainfo[ $key ] = $value;

        $this->table->update(
            [$this->table->primary => $trackid],
            [ COLUMN_METAINFO => Format::largeText( json_encode( $metainfo ) )]
        );
    }

    /**
     * @param $trackid
     * @param bool $array
     * @return mixed
     */

    public function streams( $trackid, $array=true )
    {

        return( json_decode( Format::decodeLargeText( $this->get( $trackid )->streams ), $array ) );
    }

    /**
     * @param $userid
     * @return \Illuminate\Support\Collection
     */

    public function user( $userid )
    {

        return( $this->table->user( $userid ) );
    }

    /**
     * @param $trackid
     * @param $streamtype
     * @param $streamurl
     * @throws \RuntimeException
     */

    public function addStream( $trackid, $streamtype, $streamurl )
    {

        $streams = $this->streams( $trackid );

        if( isset( $streams[ $streamtype ] ) )
            throw new \RuntimeException("Stream type already set");

        $streams[ $streamtype ] = $streamurl;
        $this->updateStreams( $trackid, $streams );
    }

    /**
     * @param $trackid
     * @param $streamtype
     * @throws \RuntimeException
     */

    public function removeStream( $trackid, $streamtype )
    {

        $streams = $this->streams( $trackid );

        if( isset( $streams[ $streamtype ] ) == false  )
            throw new \RuntimeException("Stream type missing");

        unset( $streams[ $streamtype ] );
        $this->updateStreams( $trackid, $streams );
    }

    /**
     * @param $trackid
     * @param $streams
     */

    public function updateStreams( $trackid, $streams )
    {

        $this->table->update([$this->table->primary => $trackid ], ["streams" => Format::largeText( $streams ) ] );
    }

    /**
     * @param int $userid
     * @param array $streams
     * @param string|null $trackname
     * @param $metainfo
     * @param null $colour
     * @param string|null $privacy
     * @return int
     * @throws \RuntimeException
     */

    public function create( int $userid, array $streams, string $trackname=null, $metainfo, $colour=null, string $privacy=null )
    {

        if( $trackname == null )
            $trackname = $this->generate();

        if( $colour == null )
            $colour = Format::colour();

        if( $privacy == null )
            $privacy = TRACK_PRIVACY_PUBLIC;

        return( $this->table->insert([
            "userid"        => $userid,
            "trackname"     => $trackname,
            "streams"       => Format::largeText( json_encode( $streams ) ),
            COLUMN_METAINFO => Format::largeText( json_encode( $metainfo ) ),
            "colour"        => $colour,
            "privacy"       => $privacy,
            "creation"      => Format::timestamp()
        ], false ) );
    }

    /**
     * @return string
     */

    private function generate()
    {

        $username = TRACK_PREFIX;
        $digits = "";

        for( $i = 0; $i < TRACK_DIGITS; $i++ )
        {

            $digits = $digits . rand( TRACK_RND_MIN, TRACK_RND_MAX );
        }

        return( $username . $digits );
    }
}