<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 13/08/2018
 * Time: 00:40
 */

namespace Colourspace\Framework\Verifications;


use Colourspace\Framework\Interfaces\VerificationInterface;
use Colourspace\Framework\Verifications;
use Colourspace\Framework\Util\Collector;

abstract class Base implements VerificationInterface
{

    /**
     * @var Verifications
     */

    protected $verifications;

    /**
     * Email constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->verifications = Collector::new("Verifications");
    }

    /**
     * @param $data
     * @return bool
     * @throws \RuntimeException
     */

    public function verify($data)
    {

        $key = @$this->verifications->parse( $data[ "key" ] );

        if( $key == false )
            return false;
        else
        {

            if( $this->verifications->check( $key[0], $key[1] ) == false )
                return false;

            $this->verifications->delete( $key[ 0 ] );
        }

        return true;
    }
}