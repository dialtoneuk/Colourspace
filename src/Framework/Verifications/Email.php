<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 14/07/2018
 * Time: 22:06
 */

namespace Colourspace\Framework\Verifications;



use Colourspace\Framework\User;
use Colourspace\Framework\Util\Collector;
use Colourspace\Framework\Util\Mailer;


class Email extends Base
{

    /**
     * @var Mailer;
     */

    protected $mailer;

    /**
     * @var User
     */

    protected $user;

    /**
     * Email constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        $this->mailer = Collector::new("Mailer", "Colourspace\\Framework\\Util\\");
        $this->user = Collector::new("User");

        parent::__construct();
    }

    /**
     * @param $userid
     * @return array|bool|string
     * @throws \RuntimeException
     * @throws \Exception
     */

    public function process( $userid )
    {

        if( $this->user->exist( $userid ) == false )
            throw new \RuntimeException("User is invalid, has it been registered first?");

        if( $this->verifications->hasType( $userid, VERIFICATIONS_TYPE_EMAIL ) )
            return false;

        $result = $this->verifications->create( $userid, VERIFICATIONS_TYPE_EMAIL );

        if( empty( $result ) )
            throw new \RuntimeException("Invalid result");

        $user = $this->user->get( $userid );

        if( filter_var( $user->email, FILTER_VALIDATE_EMAIL ) == false )
            throw new \RuntimeException("Email address given is not a valid email");

        if( $this->mailer->exist( MAILER_VERIFY_TEMPLATE ) == false )
            throw new \RuntimeException("Template not found");

        $body = $this->mailer->parse( [
            "key" => $this->verifications->format( $result ),
            "username" => $user->username
        ], MAILER_VERIFY_TEMPLATE );

        $this->mailer->send( $user->email, "Verify your email", $body);

        return( true );
    }
}