<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 14/07/2018
 * Time: 22:06
 */

namespace Colourspace\Framework\Verifications;


class Mobile extends Base
{

    /**
     * @param $userid
     * @return array|bool|string
     * @throws \RuntimeException
     * @throws \Exception
     */

    public function process( $userid )
    {

        if( $this->verifications->hasType( $userid, VERIFICATIONS_TYPE_MOBILE ) )
            return false;

        $result = $this->verifications->create( $userid, VERIFICATIONS_TYPE_MOBILE );

        if( empty( $result ) )
            throw new \RuntimeException("Invalid result");

        //TODO: This is where I would basically send the mobile text

        return( $this->verifications->format( $result ) );
    }
}