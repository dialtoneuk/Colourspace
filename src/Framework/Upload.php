<?php

namespace Colourspace\Framework;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 29/08/2018
 * Time: 21:44
 */

use Colourspace\Container;
use Colourspace\Framework\Interfaces\Upload as UploadInterface;
use Colourspace\Framework\Util\Debug;
use Colourspace\Framework\Util\FileUpload;
use Colourspace\Framework\Util\Format;
use Colourspace\Framework\Util\Conventions\UploadData;
use Delight\FileUpload\File;

abstract class Upload implements UploadInterface
{

    /**
     * @var FileUpload
     */

    private $fileupload;

    /**
     * @var bool|Session
     */

    protected $session = false;

    /**
     * @var array
     */

    protected $previous_extensions = [];

    /**
     * Upload constructor.
     */

    public function __construct()
    {

        $this->fileupload = new FileUpload();
    }

    /**
     * @param $data
     * @param $userid
     * @return mixed|void
     */

    public function before( UploadData $data, $userid)
    {

        if( Container::exist("application") == false )
            throw new \RuntimeException("no application instance?");

        $this->session = Container::get("application")->session;

        if( Debug::isEnabled() )
            Debug::message("before called");
    }

    /**
     * @param File $file
     * @param UploadData $data
     * @param $userid
     * @return void
     */

    public function after(File $file, UploadData $data, $userid)
    {

        if( Debug::isEnabled() )
            Debug::message("after called");

        if( $file->exists() == false )
            throw new \RuntimeException("File does not exist after upload an error probably occured");
    }

    /**
     * @param $data
     * @param $userid
     * @return bool
     */

    public function authenticate( UploadData $data, $userid): bool
    {

        if( Debug::isEnabled() )
            Debug::message("authenticate called");

        if( $this->isLoggedIn() == false )
            return( false );

        return( true );
    }

    /**
     * @param UploadData $data
     * @param $userid
     * @return bool|File
     * @throws \Exception
     */

    public function upload( UploadData $data, $userid)
    {

        if( Debug::isEnabled() )
            Debug::message("upload called");

        $result = $this->save( $data->filename );

        if( $result == false )
            throw $this->getLastError();

        return( $result );
    }

    /**
     * @param array $extensions
     * @param bool $merge_previous
     */

    protected final function setExtensions( array $extensions, $merge_previous=true )
    {

        if( $merge_previous )
            if( empty( $this->previous_extensions ) == false )
                $extensions = array_merge( $extensions, $this->previous_extensions );

       $this->fileupload->setAllowedExtensions( $extensions );
       $this->previous_extensions = $extensions;
    }

    /**
     * @param null $file_name
     * @return bool|\Delight\FileUpload\File
     */

    protected final function save( $file_name=null )
    {

        if( $file_name == null )
            $file_name = Format::filename();

        return( $this->fileupload->save( $file_name ) );
    }

    /**
     * @return \Exception|string
     */

    protected final function getLastError()
    {

        if( FileUpload::hasLastError() == false )
            throw new \RuntimeException("does not have error");

        return( FileUpload::getLastError() );
    }

    /**
     * @param null $file_size
     */

    protected final function setMaxFileSize( $file_size=null )
    {

        if( $file_size == null )
            $file_size = UPLOADS_MAX_SIZE_GLOBAL;

        $this->fileupload->setMaxFileSize( $file_size );
    }


    /**
     * @return bool
     */

    protected final function isLoggedIn()
    {

        if( $this->session == false )
            return false;

        if( $this->session->isActive() == false )
            return false;

        return( $this->session->isLoggedIn() );
    }
}