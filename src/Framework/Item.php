<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 18/07/2018
 * Time: 20:53
 */

namespace Colourspace\Framework;


use Colourspace\Container;
use Colourspace\Framework\Interfaces\Item as ItemInterface;


abstract class Item implements ItemInterface
{

    /**
     * @var Session
     */

    protected $session;

    /**
     * @var Group
     */

    protected $group;

    /**
     * @var User
     */

    protected $user;

    /**
     * @param bool $autocreate
     * @throws \RuntimeException
     */

    public function before( $autocreate=true )
    {

        if( Container::exist("application") == false )
            throw new \RuntimeException("Needs application");

        $this->session = Container::get("application")->session;

        if( $autocreate )
        {
            $this->group = new Group();
            $this->user = new User();
        }
    }

    /**
     * @param $userid
     * @param $data
     * @return bool
     */

    public function authentication( $userid, $data )
    {

        return( true );
    }

    /**
     * @param $userid
     * @param $data
     * @return bool
     */

    public function purchase( $userid, $data )
    {

        return( true );
    }

    /**
     * @param $colour
     * @return string
     * @throws \RuntimeException
     */

    public function processColour( $colour )
    {

        $exp = explode(".", $colour ) ;

        if( count( $exp ) == 1 )
        {

            $exp = explode(",", $colour ) ;

            if( count( $exp ) == 1 )
                throw new \RuntimeException("Colour is invalid");
        }

        foreach( $exp as $key=>$item )
        {


            if( is_numeric( $item ) == false )
                throw new \RuntimeException("Colour is invalid");

            if( $item > 255 || $item < 0  )
                $exp[ $key ] = 255;

            if( $item == -0 )
                $exp[ $key ] = 0;
        }


        if( count( $exp ) < 3 )
            throw new \RuntimeException("Colour is too long" ) ;

        return( implode(",", $exp ) );
    }

    /**
     * @param $requirements
     * @param $data
     * @return bool
     */

    private function check( $requirements, $data )
    {

        foreach( $requirements as $requirement )
            if( isset( $data[ $requirement ] ) == false )
                return false;

        return true;
    }
}