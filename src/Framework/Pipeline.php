<?php
namespace Colourspace\Framework;

/**
 * Class Pipeline
 *
 * Automatically created at: 2018-09-01 21:37:24
 */

use Colourspace\Framework\Util\Collection;
use Colourspace\Framework\Util\Convention;
use Colourspace\Framework\Util\Conventions\AmbiguousData;

class Pipeline extends Collection
{

    /**
     * Pipeline constructor.
     * @param $filepath
     * @param $namespace
     * @param bool $auto_create
     */

    public function __construct( $filepath , $namespace, bool $auto_create = true)
    {

        parent::__construct( $filepath , $namespace, $auto_create);
    }

    /**
     * Returns a new convention class.
     * Remember to change the return type to the correct convention.
     *
     * @param array $values
     * @return Convention
     */

    public static function dataInstance( array $values )
    {

        return( new AmbiguousData( $values ) );
    }
}