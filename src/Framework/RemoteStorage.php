<?php

namespace Colourspace\Framework;

/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 31/08/2018
 * Time: 14:34
 */

use Colourspace\Framework\Util\Collection;

class RemoteStorage extends Collection
{

    public function __construct($filepath, $namespace, bool $auto_create = true)
    {
        parent::__construct($filepath, $namespace, $auto_create);
    }
}