<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 14/07/2018
 * Time: 21:55
 */

namespace Colourspace\Framework;


use Colourspace\Framework\Interfaces\VerificationInterface;
use Colourspace\Framework\Util\Constructor;

class VerificationsManager
{

    /**
     * @var Constructor
     */

    protected $constructor;

    /**
     * Verifications constructor.
     * @param bool $auto_init
     * @param null $namespace
     * @param null $root
     * @throws \RuntimeException
     */

    public function __construct( $auto_init=true, $namespace=null, $root=null )
    {

        if( $namespace == null )
            $namespace = VERIFICATIONS_NAMESPACE;

        if( $root == null )
            $root = VERIFICATIONS_ROOT;

        $this->constructor = new Constructor( $root, $namespace );

        if( $auto_init )
            $this->initialize();
    }

    /**
     * @param $class_name
     * @param $data
     * @return mixed
     * @throws \RuntimeException
     */

    public function process( $class_name, $data )
    {

        if( $this->hasInit() == false )
            $this->initialize();

        if( empty( $data ) || is_array( $data ) == false )
            throw new \RuntimeException("Invalid data");

        if( $this->exist( $class_name ) == false )
            throw new \RuntimeException("Class does not exist: " . $class_name );

        $class = $this->get( $class_name );

        if( $class instanceof VerificationInterface == false )
            throw new \RuntimeException("Invalid class");

        $result = $class->process( $data );

        if( empty( $result ) || is_array( $result ) == false )
            throw new \RuntimeException("Invalid result");

        return( $result );
    }

    /**
     * @param $class_name
     * @param $data
     * @return bool
     * @throws \RuntimeException
     */

    public function verify( $class_name, $data )
    {

        if( $this->hasInit() == false )
            $this->initialize();

        if( empty( $data ) || is_array( $data ) == false )
            throw new \RuntimeException("Invalid data");

        if( $this->exist( $class_name ) == false )
            throw new \RuntimeException("Class does not exist: " . $class_name );

        $class = $this->get( $class_name );

        if( $class instanceof VerificationInterface == false )
            throw new \RuntimeException("Invalid class");

        $result = $class->verify( $data );

        if( empty( $result ) || is_bool( $result ) == false )
            throw new \RuntimeException("Invalid result");

        return( $result );
    }

    private function hasInit()
    {

        if( empty( $this->constructor ) )
            return false;

        if( empty( $this->constructor->getAll() ) )
            return false;

        return true;
    }

    /**
     * @param $class_name
     * @return mixed
     */

    public function getVerificationClass( $class_name )
    {

        return( $this->get( $class_name ) );
    }

    /**
     * @param $class_name
     * @return bool
     */

    public function exist( $class_name )
    {

        if( $this->constructor->exist( $class_name ) == false )
            return false;

        return true;
    }

    /**
     * @throws \RuntimeException
     */

    public function initialize()
    {

        $result = $this->constructor->createAll();

        if( empty( $result ) )
            throw new \RuntimeException("Empty");
    }

    /**
     * @param $class_name
     * @return mixed
     */

    private function get( $class_name )
    {

        return( $this->constructor->get( $class_name ) );
    }
}