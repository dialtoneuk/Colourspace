<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 08/08/2018
 * Time: 22:22
 */

namespace Colourspace\Framework;


use Colourspace\Framework\Util\DirectoryOperator;

class UserPermissions
{

    /**
     * @var DirectoryOperator
     */

    protected $directory;

    /**
     * UserPermissions constructor.
     * @throws \RuntimeException
     */

    public function __construct()
    {

        if( file_exists( COLOURSPACE_ROOT . USER_PERMISSIONS_ROOT ) == false )
            throw new \RuntimeException("Please run auto migrate");

        $this->directory = new DirectoryOperator( USER_PERMISSIONS_ROOT );
    }

    /**
     * @param $userid
     * @param string $group
     * @throws \RuntimeException
     */

    public function create( $userid, $group="default" )
    {

        if( file_exists( COLOURSPACE_ROOT . GROUPS_ROOT . strtolower( $group ) . ".json" ) == false )
            throw new \RuntimeException("Invalid group must refer to the file name and not include .json");

        $group = json_decode( file_get_contents( COLOURSPACE_ROOT . GROUPS_ROOT . strtolower( $group ) . ".json" ), true );

        if( isset( $group["flags"] ) == false )
            throw new \RuntimeException("Invalid group fucked up array");

        $this->save( $userid, $group["flags"] );
    }

    /**
     * @param $userid
     * @return bool
     */

    public function remove( $userid )
    {

        return @unlink( COLOURSPACE_ROOT . USER_PERMISSIONS_ROOT . $userid . ".json");
    }

    /**
     * @param $userid
     * @param array $values
     */

    public function updateMultiple( $userid, array $values )
    {

        $array = $this->get( $userid );

        foreach( $values as $key=>$value )
            if( isset( $array[ $key ] ) )
                $array[ $key ] = $value;

        $this->save( $userid, $array );
    }

    /**
     * @param $userid
     * @param $flag
     * @param $value
     * @throws \RuntimeException
     */

    public function update( $userid, $flag, $value )
    {

        $array = $this->get( $userid );

        if( isset( $array[ $flag ] ) == false )
            throw new \RuntimeException("Flag needs to be valid");

        $array[ $flag ] = $value;

        $this->save( $userid, $array );
    }

    /**
     * @param $userid
     * @param $flag
     * @return bool
     */

    public function check( $userid, $flag )
    {

        $array = $this->get( $userid );

        if( isset( $array[ $flag ] ) == false )
            return false;

        return true;
    }

    /**
     * @param $userid
     * @return bool
     */

    public function exist( $userid )
    {

        if( $this->directory->has( $userid ) == false )
            return false;

        return true;
    }

    /**
     * @param $userid
     * @return array
     */

    public function get( $userid )
    {

        return( $this->directory->getJson( $userid, ".json", true ) );
    }

    /**
     * @param $userid
     * @param $array
     */

    public function save($userid, $array )
    {

        file_put_contents( COLOURSPACE_ROOT . $this->directory->path() . $userid . ".json", json_encode( array_merge(["updated" => time() ], $array ) ) );
    }

}