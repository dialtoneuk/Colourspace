<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 16:53
 */

namespace Colourspace\Framework;


use Colourspace\Framework\Interfaces\ReturnsInterface;

use Colourspace\Framework\Interfaces\ViewInterface;

use Colourspace\Framework\Util\Collector;


abstract class View implements ViewInterface
{
    /**
     * @var Model
     */

    public $model;

    /**#
     * @var ReturnsInterface
     */

    protected $type;

    /**
     * View constructor.
     * @param string $type
     * @throws \RuntimeException
     */

    public function before( $type="FlightRender" )
    {

        $this->type = Collector::new($type, "Colourspace\\Framework\\Types\\");
    }

    /**
     * @param Model $model
     */

    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param null $template
     * @param null $payload
     * @return ReturnsInterface|mixed
     */

    public function get( $template=null, $payload=null )
    {

        $this->type->add([
            "template"    => $template,
            "model"     => $this->model->toArray()
        ]);

        if( $payload !== null )
        {

            $this->type->add(["page" => $payload ]);
        }
        
        return( $this->type );
    }

    /**
     * @return \stdClass
     */

    public function object()
    {

        return( $this->model->object );
    }

    /**
     * @return mixed
     */

    private function model()
    {

        return( $this->model->toArray() );
    }
}