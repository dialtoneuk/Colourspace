<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 15:40
 */

namespace Colourspace\Framework;


use Colourspace\Framework\Util\FileOperator;

class Router
{

    /**
     * The actual; file
     * @var FileOperator
     */

    protected $routes;

    /**
     * The live routes which are fed to Flight
     * @var array
     */

    public $live_routes;

    /**
     * Router constructor.
     * @param bool $auto_initialize
     * @throws \RuntimeException
     */

    public function __construct( $auto_initialize=true )
    {

        $this->routes = new FileOperator( MVC_ROUTE_FILE, true );

        if( $this->routes->isEmpty() )
            throw new \RuntimeException('Route file is empty');

        if( $auto_initialize )
            $this->initialize();
    }

    /**
     * @throws \RuntimeException
     */

    public function initialize()
    {

        $routes = $this->routes->decodeJSON( true );

        if( empty( $routes ) )
            throw new \RuntimeException('Routes are empty, check file');

        foreach ( $routes as $key=>$route )
        {

            if( $this->check( $route ) == false )
                throw new \RuntimeException('Route failed check at index: ' . $key );

            if( isset( $this->live_routes[ $route['url'] ] ) )
                continue;

            if( $this->hasPayload( $route ) )
            {

                if( $this->checkPayload( $route ) == false )
                    throw new \RuntimeException("Invalid payload in route");
                else
                    $payload = $route["payload"];
            }
            else
                $payload = null;


            $this->live_routes[ $route['url'] ] = [
                "mvc"       => $this->lower( $route['mvc'] ),
                "template"  => $route["template"],
                "payload"   => $payload
            ];
        }
    }

    /**
     * @return bool
     */

    public function test()
    {

        if( empty( $this->live_routes ) )
            return false;

        foreach( $this->live_routes as $route )
        {

            if( empty( $route ) )
                return false;
        }

        return true;
    }

    /**
     * @param $url
     * @return bool
     */

    public function hasRoute( $url )
    {

        return( isset( $this->live_routes[ $url ] ) );
    }

    /**
     * @param $url
     * @return mixed
     */

    public function getRoute( $url )
    {

        return( $this->live_routes[ $url ] );
    }

    /**
     * @param $url
     * @return mixed
     */

    public function getRoutePayload( $url )
    {

        returN( $this->live_routes[ $url ]['mvc'] );
    }

    /**
     * Changes the keys to be lower case
     * @param array $mvc
     * @return array
     */

    private function lower( array $mvc )
    {

        foreach ( $mvc as $key=>$value )
            $mvc[ strtolower( $key ) ] = $value ;

        return $mvc;
    }

    /**
     * @param $route
     * @return bool
     */

    private function checkPayload( $route )
    {

        if( isset( $route["payload"] ) == false )
            return false;

        return true;
    }

    /**
     * @param $route
     * @return bool
     */

    private function hasPayload( $route )
    {

        if( isset( $route["payload"] ) == false )
            return false;

        return true;
    }

    /**
     * @param $route
     * @return bool
     */

    private function check( $route )
    {

        if( isset( $route['url'] ) == false )
            return false;

        if( isset( $route['template'] ) == false )
            return false;

        if( isset( $route['mvc'] ) == false )
            return false;

        if( count( $route['mvc'] ) < 3 )
            return false;

        foreach( $route['mvc'] as $key=>$type )
        {

            if( strtolower( $key ) != ( MVC_TYPE_MODEL or MVC_TYPE_VIEW or MVC_TYPE_CONTROLLER ) )
                return false;
        }

        return true;
    }
}