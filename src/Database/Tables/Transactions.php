<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 18/07/2018
 * Time: 20:00
 */

namespace Colourspace\Database\Tables;

use Colourspace\Database\Table;

class Transactions extends Table
{

    /**
     * @var string
     */

    public $primary = "transactionid";

    /**
     * @return string
     */

    public function name()
    {

        return "purchases";
    }

    /**
     * @return array
     */

    public function map()
    {

        return([
            $this->primary      => FIELD_TYPE_INCREMENTS,
            COLUMN_USERID       => FIELD_TYPE_INT,
            "value"             => FIELD_TYPE_INT,
            "type"              => FIELD_TYPE_INT,
            "item"              => FIELD_TYPE_STRING,
            "when"              => FIELD_TYPE_TIMESTAMP
        ]);
    }
}