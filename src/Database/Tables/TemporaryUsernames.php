<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 30/06/2018
 * Time: 21:44
 */

namespace Colourspace\Database\Tables;


use Colourspace\Database\Table;

class TemporaryUsernames extends Table
{

    /**
     * @var string
     */

    public $primary = COLUMN_SESSIONID;

    /**
     * @return string
     */

    public function name()
    {

        return "temporary_usernames";
    }

    /**
     * @return array
     */

    public function map()
    {

        return([
            "*" . $this->primary    => FIELD_TYPE_STRING,
            "username"              => FIELD_TYPE_STRING,
            "ipaddress"             => FIELD_TYPE_IPADDRESS,
            COLUMN_CREATION         => FIELD_TYPE_TIMESTAMP
        ]);
    }
}