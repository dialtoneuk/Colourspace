<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/08/2018
 * Time: 23:11
 */

namespace Colourspace\Database\Tables;


use Colourspace\Database\Table;

class Audits extends Table
{

    /**
     * @var string
     */

    public $primary ="auditid";

    /**
     * @return string
     */

    public function name()
    {

        return "audits";
    }

    /**
     * @return array
     */

    public function map()
    {

        return([
            $this->primary  => FIELD_TYPE_INCREMENTS,
            "type"          => FIELD_TYPE_STRING,
            "userid"        => FIELD_TYPE_INT,
            "admin"         => FIELD_TYPE_INT,
            COLUMN_METAINFO => FIELD_TYPE_JSON,
            COLUMN_CREATION => FIELD_TYPE_TIMESTAMP
        ]);
    }
}