<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 29/06/2018
 * Time: 23:40
 */

namespace Colourspace\Database\Tables;

use Colourspace\Database\Table;

class Users extends Table
{

    /**
     * @var string
     */

    public $primary = COLUMN_USERID;

    /**
     * @return string
     */

    public function name()
    {

        return "users";
    }

    /**
     * The map for the users table
     *
     * @return array
     */

    public function map()
    {

        return([
            $this->primary      => FIELD_TYPE_INCREMENTS,
            "username"          => FIELD_TYPE_STRING,
            "password"          => FIELD_TYPE_STRING,
            "email"             => FIELD_TYPE_STRING,
            "salt"              => FIELD_TYPE_STRING,
            "group"             => FIELD_TYPE_STRING,
            "colour"            => FIELD_TYPE_STRING,
            COLUMN_CREATION     => FIELD_TYPE_TIMESTAMP
        ]);
    }
}