<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 29/06/2018
 * Time: 23:40
 */

namespace Colourspace\Database\Tables;

use Colourspace\Database\Table;

class Sessions extends Table
{

    /**
     * @var string
     */

    public $primary = COLUMN_SESSIONID;

    /**
     * @return string
     */

    public function name()
    {

        return "sessions";
    }

    /**
     * The map for the users table
     *
     * @return array
     */

    public function map()
    {

        return([
            COLUMN_USERID           => FIELD_TYPE_INT,
            COLUMN_SESSIONID        => FIELD_TYPE_STRING,
            "ipaddress"             => FIELD_TYPE_IPADDRESS,
            COLUMN_CREATION         => FIELD_TYPE_TIMESTAMP
        ]);
    }
}