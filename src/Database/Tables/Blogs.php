<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 06/07/2018
 * Time: 01:16
 */

namespace Colourspace\Database\Tables;

use Colourspace\Database\Table;

class Blogs extends Table
{

    /**
     * @var string
     */

    public $primary = "blogid";

    /**
     * @return string
     */

    public function name()
    {

        return "blog";
    }

    /**
     * The map for the users table
     *
     * @return array
     */

    public function map()
    {

        return([
            $this->primary      => FIELD_TYPE_INCREMENTS,
            COLUMN_USERID       => FIELD_TYPE_STRING,
            "content"           => FIELD_TYPE_JSON,
            COLUMN_CREATION     => FIELD_TYPE_TIMESTAMP
        ]);
    }
}