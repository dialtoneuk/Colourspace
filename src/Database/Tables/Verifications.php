<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 12/07/2018
 * Time: 22:16
 */

namespace Colourspace\Database\Tables;

use Colourspace\Database\Table;


class Verifications extends Table
{

    /**
     * @var string
     */

    public $primary = "verifyid";

    /**
     * @return string
     */

    public function name()
    {

        return "verifications";
    }

    /**
     * The map for the users table
     *
     * @return array
     */

    public function map()
    {

        return([
            $this->primary      => FIELD_TYPE_INCREMENTS,
            COLUMN_USERID       => FIELD_TYPE_STRING,
            "type"              => FIELD_TYPE_STRING,
            "data"              => FIELD_TYPE_JSON
        ]);
    }
}