<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 18/07/2018
 * Time: 20:00
 */

namespace Colourspace\Database\Tables;

use Colourspace\Database\Table;

class Balances extends Table
{

    /**
     * @var string
     */

    public $primary = "balanceid";

    /**
     * @return string
     */

    public function name()
    {

        return "balances";
    }

    /**
     * @return array
     */

    public function map()
    {

        return([
            $this->primary      => FIELD_TYPE_INCREMENTS,
            COLUMN_USERID       => FIELD_TYPE_INT,
            "amount"            => FIELD_TYPE_INT,
            COLUMN_CREATION     => FIELD_TYPE_TIMESTAMP
        ]);
    }
}