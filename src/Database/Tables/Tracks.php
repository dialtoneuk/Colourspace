<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 05/07/2018
 * Time: 20:03
 */

namespace Colourspace\Database\Tables;


use Colourspace\Database\Table;


class Tracks extends Table
{

    /**
     * @var string
     */

    public $primary = COLUMN_TRACKID;

    /**
     * @return string
     */

    public function name()
    {

        return "tracks";
    }

    /**
     * @return array
     */

    public function map()
    {

        return([
            $this->primary          => FIELD_TYPE_INCREMENTS,
            COLUMN_USERID           => FIELD_TYPE_INT,
            "trackname"             => FIELD_TYPE_STRING,
            "streams"               => FIELD_TYPE_STRING,
            COLUMN_METAINFO         => FIELD_TYPE_JSON,
            "colour"                => FIELD_TYPE_STRING,
            "privacy"               => FIELD_TYPE_STRING,
            COLUMN_CREATION         => FIELD_TYPE_TIMESTAMP
        ]);
    }
}