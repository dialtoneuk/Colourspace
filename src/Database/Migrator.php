<?php
namespace Colourspace\Database;

use Colourspace\Container;
use Colourspace\Database\Interfaces\Table;
use Colourspace\Framework\Util\Constructor;
use Colourspace\Framework\Util\Debug;
use Illuminate\Database\Schema\Blueprint;

class Migrator
{

    protected $constructor;
    protected $connection;

    /**
     * Migrator constructor.
     * @param bool $auto_initialize
     * @throws \RuntimeException
     */

    public function __construct( $auto_initialize=true )
    {

        if( Container::exist("application") == false )
            throw new \RuntimeException("Application has not been initialized");

        $this->connection = Container::get('application')->connection->connection;

        $this->constructor = new Constructor( TABLES_ROOT, TABLES_NAMESPACE );

        if( $auto_initialize )
            $this->initialize();
    }

    /**
     * @throws \RuntimeException
     */

    public function initialize()
    {

        if( file_exists( COLOURSPACE_ROOT . TABLES_ROOT ) == false )
            throw new \RuntimeException("Tables do not exist");

        if( is_file( COLOURSPACE_ROOT . TABLES_ROOT  ) )
            throw new \RuntimeException("Must be the locaiton of a folder");

        $this->constructor->createAll();
    }

    /**
     * @throws \RuntimeException
     */

    public function process()
    {

        foreach ( $this->constructor->getAll() as $name=>$class )
        {

            if( Debug::isCMD() )
                Debug::echo( "Using class: $name ", 6);

            if( DEBUG_ENABLED )
                Debug::message('Creating table for ' . $name );

            if( $class instanceof Table == false )
                throw new \RuntimeException("Incorrect class");

            /** @var Table $class */
            $table_name = strtolower( $class->name() );

            if( empty( $class->map() ) )
                throw new \RuntimeException("No migrator map in class");

            if( $this->tableExists( $table_name ) )
                continue;

            $this->create( $table_name, $class->map() );

            if( DEBUG_ENABLED )
                Debug::message('Finished creating table for ' . $name );
        }
    }

    /**
     * @param string $table_name
     * @param array $table_map
     */

    private function create( string $table_name, array $table_map )
    {

        $this->connection->getSchemaBuilder()->create( $table_name, function( Blueprint $table ) use ( $table_map ){

            foreach ( $table_map as $coloum=>$type )
            {

                if( substr( $coloum, 0, 1 ) == "*" )
                {

                    $coloum = str_replace("*", "", $coloum );
                    $primary = true;
                }
                else
                    $primary = false;

                if( substr( $coloum, 0, 1 ) == "!" )
                {

                    $coloum = str_replace("!", "", $coloum );
                    $unique = true;
                }
                else
                    $unique = false;

                if( Debug::isCMD() )
                    Debug::echo( "Create column: $coloum with type $type ", 6);

                switch ( $type )
                {

                    case FIELD_TYPE_STRING:
                        $table->string( $coloum );
                        break;
                    case FIELD_TYPE_INT:
                        $table->integer( $coloum );
                        break;
                    case FIELD_TYPE_TIMESTAMP:
                        $table->timestamp( $coloum );
                        break;
                    case FIELD_TYPE_INCREMENTS:
                        $table->increments( $coloum );
                        break;
                    case FIELD_TYPE_DECIMAL:
                        $table->decimal( $coloum );
                        break;
                    case FIELD_TYPE_IPADDRESS:
                        $table->ipAddress( $coloum );
                        break;
                    case FIELD_TYPE_JSON:
                        $table->longText( $coloum );
                        break;
                }

                if( $primary )
                {

                    if( Debug::isCMD() )
                        Debug::echo( "Primary set: $coloum", 7);

                    $table->primary( $coloum );
                }

                if( $unique )
                {

                    if( Debug::isCMD() )
                        Debug::echo( "Unique set: $coloum", 7);

                    $table->unique( $coloum );
                }
            }
        });
    }

    /**
     * @param $table_name
     * @return bool
     */

    private function tableExists( $table_name )
    {

        if( $this->connection->getSchemaBuilder()->hasTable( $table_name ) )
        {

            if( Debug::isCMD() )
                Debug::echo( "Table exists: $table_name ", 5);

            return true;
        }

        return false;
    }
}