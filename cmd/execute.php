<?php
require_once "vendor/autoload.php";

/**
 * Namespaces
 */

use Colourspace\Framework\Util\Scripts;
use Colourspace\Framework\Util\Debug;
use Colourspace\Container;

//Set CMD
Debug::setCMD();

//Include the index to get our globals
include_once "index.php";

Debug::echo("Colourspace Script Executer");
Debug::echo("[ written by lewis lancaster in 2018 ]\n");

try
{


    Debug::echo("Script Preload \n" );

    if( count( $argv ) == 1 )
        throw new Error("Not enough arguments, please provide a script");

    $scripts = new Scripts( $argv );

    Debug::echo("\n Executing " . $scripts->script() . "\n" );

    if( $scripts->exists( $scripts->script() ) == false )
        throw new Error("Script does not exist: " . $scripts->script() . ". Please execute php -f cmd/execute.php help script=false if you are having trouble." );

    //Globalizing Scripts
    Container::add("scripts", $scripts );

    //Execute
    $scripts->execute( $scripts->script() );
}
catch ( Error $error )
{

    Debug::echo( "[Critical Error " . $error->getFile() . " " . $error->getLine() . "] : " . $error->getMessage() . "" );

    if( empty( $scripts ) )
        exit( 1 );

    Debug::echo("\n You can type php -f cmd/execute.php help script=" . $scripts->script() . " for help!");

    exit( 1 );
}

